(page
 :title "Wahirllizmo - A Text Adventure"
 :style
 "html {
    background: black;
    background: url(/css/p/stars.jpg);}
body{
    background: #0E364D;
    max-width: 50em;
    margin: 1em auto;
    font: 1.5em/1.6em sans-serif;
    color: white;
    border: 5px inset  white;
    border-radius: 10px;
    text-align: center;}
img {
    width: 100%;
    height: auto;}

a{color: magenta; text-decoration: none;}
a:hover {text-decoration: underline;}

p {
    text-align: left;
    padding: .5em;}
pre {
    text-align: left;
    padding: .5em;
    margin: .5em;
    border-radius: 10px;
    background: black;
    color: lime;}"
 :body
 `((h1 Wahirllizmo)
   (h2 The Adventure.)
   (p "Wahirllizmo is a text-based interactive adventure game written
    in the C programming language in a single day. You play as
    yourself. You wake up one day and you are asked to make a
    sandwich. The story begins there as you go ruin a few people's
      days.")
   
   (p "The name is just a bunch of letters that sounded okay
    together. There is no reason for the naming, although, the dog is
    named Wahirllizmo. There is no ultimate meaning to this name. I
    guess it kind of sounds like Whirl like a hellicopter, and
    gizmo or wizmo like a mechanical gadget, which the dog is sort
      of both.")
   
   (p "Okay, fine, I'll admit it, this isn't exactly a turing award
    winning program. I just felt like writing some trivial
    C. Nevertheless, I enjoyed making and running this game. :)")
   
   (h2 Download)
   (pre "git clone https://gitlab.com/MutoShack/c-text-game.git")
   (p ,(a  "https://gitlab.com/MutoShack/c-text-game"
	   "See the source code!"))

   ,(img "/css/wah.png" "Thanks :)")))
