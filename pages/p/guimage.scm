(page
 :title "GuiMage - Image Gallery"
 :body
 `(,(img "/css/p/guimage.png" "Showcase your photos with...")
   (h1 "GuiMage!")
   (h2 "A Lispy gallery generator!")
   (p "GuiMage (Guile-Image) is a program written in Guile Scheme
    that takes all the image files from a directory and puts them in
    an HTML gallery! This is very similar to, and pairs nicely with"
      ,(a  "https://muto.ca/projects/txtblog/index.html"
	   "TXTBLOG"))

   (pre
    "$ ls
img/
guimage.scm
style.css
simple.scm

$ guile guimage.scm

$ ls
guimage.scm
style.css
simple.scm
index.html

$ firefox index.html")

   (p "It's a simple program right now. There's no extremely
intuitive output or options yet, but it's a good demonstration for
anyone interested in Guile's SXML functionality.")
   (h2 "DOWNLOAD")
   (pre "git clone https://gitlab.com/MutoShack/guimage.git")
   (p ,(a  "https://gitlab.com/MutoShack/guimage"
	   "View the source code!"))))
