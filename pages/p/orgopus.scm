(page
 :title "Orgopus - The Opus Music Organizer"
 :style
 "body {
    text-align: center;
    font: 1.2em/1.4em sans-serif;
    max-width: 50em;
    margin: .5em auto;
    background: darkblue;
    color: #EEEEEE
}

p {text-align: left;}
a {color: yellow; text-decoration: none;}
a:hover {text-decoration: underline;}

pre {
    background-color: black;
    color: lightblue;
    text-align: left;
    border-radius: 10px;
    padding: 0.5em;
}"
 :body
  `(,(img "/css/p/orgopus.png" "Music Organization With...")
    (h1 "Orgopus")
    (h2 "Some Real OPUS Music organization!")
    
    (p "Orgopus (pronounced like \"Octopus\") is a script that helps you
    organize your music library so well that you'll never need to
    again! Or at least that's how it worked for me. Don't praise me
    too hard just yet though, the guys at FFMPEG and OpusComment did
    most of the complicated stuff. This program stands on the
    shoulders of those giants. Here's an example:")

    (pre "$ ls
track1.mp3
track2.mp3
track3.mp3

$ orgopus --convert mp3 opus
Delete mp3 files? [y]es [n]o
y
All old files removed!

$ ls
track1.opus
track2.opus
track3.opus

$ orgopus --replace \"track\" \"song\"
$ ls
song1.opus
song2.opus
song3.opus

$ orgopus --auto
$ ls
'01 - A Starlit Sun.opus'
'02 - Lets Rock.opus'
'03 - Radicalized Internet.opus'")
    
    (p "As you can see, Orgopus does a few different things. You can bulk
rename files, or you can let Orgopus rename files for you based on the
song metadata! It's pretty handy stuff, and it works in a pinch when
you don't want to sift through the entire FFMPEG manual or look into
AudioTag stuff!")
    
    (h2 Download)
    (pre "git clone https://gitlab.com/MutoShack/orgopus.git")
    (p ,(a "https://gitlab.com/MutoShack/orgopus"
           "See the source code!"))

    ,(img "/css/p/muto.png" "Thanks for visiting. :)")))
