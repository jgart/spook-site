(page
 #:title "Backyard - Emacs Theme"
 #:style
"body {
    max-width: 40em;
    margin: .5em auto;
    padding: 0 5%;
    text-align: center;
    background: #FFFFAA;
    color: black;
    font: 1.4em/1.5em monospace;
}

p, ul{text-align: left;}

img {
    margin-bottom: .5em;
    border-radius: 10px;
}

a {
    color: black;
    font-weight: bold;
    text-decoration: none;
    background: pink;
}

a:hover {text-decoration: underline;}

pre {
    color: black;
    background: lightblue;
    text-decoration: underline;
    text-align: left;
    padding: 0.2em;
}

h1 {
    background: lightgreen;
    padding: 5px;
}

h2      {background: white;}
#red    {background: pink;}
#green  {background: lightgreen;}
#blue   {background: lightblue;}
#white  {background: white;}
#yellow {background: yellow;}
b       {background: white;}
i       {background: lightgrey;}
u       {background: orange}
mark    {background: magenta;}"

:body
`(,(img "/css/p/backyard.png" "=)")
  (h1 "BACKYARD")
  (h2 "A Proof-of-Concept Emacs Theme")
  (p "The " (mark "Backyard Emacs colour theme") " is a
background-focused theme for Emacs. Most attributes are either "
(b bold) ", " (i italic) ", or " (u "underlined") ", but only the
background colour is changed." )
  ,(img "/css/p/back-light.png" "Backyard comes in a light theme!")
  ,(img "/css/p/back-dark.png" "Backyard comes in a dark theme!")

  (p "Aside from being fully functional for computer code, Backyard
works just fine on most of Emacs' " (mark "major modes") ", such as:")

  (ul
   (li (@ (id "red"))   "rmail")
   (li (@ (id "green"))   "dired")
   (li (@ (id "blue"))   "gnus")
   (li (@ (id "red"))   "info")
   (li (@ (id "white"))   "eshell")
   (li (@ (id "yellow")) "eww"))
  (h2 "DOWNLOAD")
  (pre "git clone https://gitlab.com/MutoShack/backyard-theme.git")
  (p ,(a "https://gitlab.com/MutoShack/backyard-theme"
	 "See the source code here!"))))
