(page
 :title "Spook - The Static Website Generator"
 :style "
body {
font: 1.2em/1.4em sans-serif;
background: #FF8ACD;
background: linear-gradient(45deg, #FFA955, #FF8ACD);
background-attachment: fixed;
max-width: 40em;
padding: 0 5%;
margin: auto;}
h1,h2{
text-align: center;}

a {color: #666666;}

pre {
background: #000000;
color: #FF8ACD;
border-radius: 5px;}

img {
max-height: 10em; width: auto;
display: block;
max-width: 90%;
margin: auto;}"
 :body
 `(,(img "/css/p/skully.png" "!SPOOK!")
   (h2 "Static Website Generator")

   (p "Spook allows you to easily organize and generate a
website. There's no funny buisiness, your website is your own.")

   (p "Just create a template file in 'spook.scm' like so:")

   (pre
"(template
 `((html
    (body
     (h2 \"Welcome to my website!\")
     ,body
     (p \"~Made with Spook~\")))))")

   (p "And then throw SXML files in the pages/ directory!")

   (pre
";; pages/index.scm
(page
 #:title \"My Webpage\"
 #:body `(p \"Hello, Spook!\"))")

   (p "You can even use the special directory pages/blog/ to generate
a blog index!")

   (pre
";; pages/blog/01-Hello.scm
(page
 #:title \"First Blog Post\"
 #:date  \"Jan 2, 2020\"
 #:body
 `((p \"This is a blog post\")
   (p \"It is nice :)\"))")

   (p "It's a simple generator, designed to be easy to grasp. As the
name implies, it's inspired by (but not a fork of) David's awesome "
      ,(a "https://dthompson.us/projects/haunt.html" "Haunt")
      " generator, which has more features than Spook")

   (p "(Please note that MY Spook has no affiliation with "
      ,(a "https://github.com/go-spook/spook"
	  "THIS")
      " spook. Man, I am really bad at picking program names!)"

   (p "Anyway I hope you enjoy using Spook!")
   (hr)
   (h2 "Download")
   (p "Clone the repository:")
   (pre "git clone https://gitlab.com/MutoShack/spook.git")
   (p ,(a "https://gitlab.com/MutoShack/spook"
	  "View the source code!")))))
