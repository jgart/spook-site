(page
 :title "RNMTH - Mathematic Problem Generator"
 :style
 "body {
    background: lightgreen;
    color: navy;
    font: 1.2em/1.4em sans-serif;
    max-width: 50em;
    margin: 1em auto;
    text-align: center;
}

a {text-decoration: none; color: blueg;}
a:hover {text-decoration: underline;}

pre {
    background: navy;
    color: lightgreen;
    padding: .5em;
    text-align: left;
}

p {text-align: left;}"
 :body
 `(,(img "/css/p/rnmth.png" "Ah, Goo Lagoon...")
   (h1 "RNMTH")
   (h2 "A Lispy Arithmetic Training Program.")
   
   (p "RNMTH (short for RaNdom MaTH) is a program that generates
random math questions for you to answer. It's helpful if you want to
brush up on your mental calculation speed and suck at coming up with
random questions yourself (like me). Here's a demo of what you can
expect from rnmth:")

   (pre "$ rnmth -r
What is: 848 + 11427
15
Incorrect!
The correct answer was: 2275

What is: 1109 / 1085
1
Correct!")
   
   (p "In this example we used the '-r' flag, meaning \"random
operator\". There's also -a, -s, -m, and -d for addition, subtraction,
multiplication, and division. In the future, I also plan to implement
stuff like roman numerals. It's fun.")
   
   (h2 Download)
   (pre "git clone https://gitlab.com/MutoShack/random-arithmetic.git")
   (p ,(a "https://gitlab.com/MutoShack/random-arithmetic"
	  "See the source code!"))

   ,(img "/css/p/mth.png" "Thanks for visiting. :)")))
