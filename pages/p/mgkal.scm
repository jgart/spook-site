(page
 :title "MGKAL - The MaGiKALphabet!"
 :style
 "body {
    background: wheat;
    color: black;
    font: 1.2em/1.4em sans-serif;
    max-width: 50em;
    margin: 1em auto;
    text-align: center;
}

a {text-decoration: none; color: blue;}
a:hover {text-decoration: underline;}

pre {
    background: brown;
    color: gold;
    padding: .5em;
    text-align: left;
}

p {text-align: left;}"
 :body
 `(,(img "/css/p/mgkal.png" "973-namuh-eht-973")
   (h1 "MGKAL")
   (h2 "The Alphanumeric Translator")
   
   (p "This program, MgKal, is short for the \"MaGiKALphabet\", a
    program that translates numbers \"1, 2, 3\", to letters \"A, B, C\"
    and vice versa. The program is written in Guile Scheme and is
    inspired by the art project"
      ,(a "http://973-namuh-eht-973.com"
	  "973-namuh-eht-973.com")
      "as the website has a lot of weird text that is encoded in this
    type of alphanumeric translation. It's also just a fun and sort of
    simple little program.")

   (p "Here's an example input/output session with MGKAL:")

   (pre "$ mgkal -e \"HELLO\" #Letters are case-insensitive
(8 5 3 3 6)

$ mgkal -d \"85336\"
HQZ   8
ENW   5
CLU   3
CLU   3
FOX   6")
   
   (p "As you can see, the decoding is a bit weird. That's because the letter
    8 could mean \"H\", \"Q\", or \"Z\", so you kind of need to play a
    search-and-find game to decode a message. Maybe in the future I'll
    make it so that the program scans a dictionary for possible words, but
    we'll see.")
   
   (h2 Download)
   (pre "git clone https://gitlab.com/MutoShack/mgkal.git")
   (p ,(a  "https://gitlab.com/MutoShack/mgkal"
	   "See the source code!"))
   ,(img "/css/p/mgk.png" "Thanks for visiting. :)")))
