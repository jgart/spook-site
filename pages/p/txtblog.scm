(define (m txt)
  `((mark ,txt)))
(page
 :title "TXTBLOG - Plaintext Blog Generator"
 :style
 "body {
    max-width: 40em;
    margin: .5em auto;
    text-align: center;
    background: pink;
    color: black;
    font: 1.2em/1.4em monospace;
}

p{text-align: left;}
a{color: blue; font-weight: bold; text-decoration: none;}
a:hover{text-decoration: underline;}

pre {
    background: navy;
    color: yellow;
    font-weight: bold;
    text-align: left;
    padding: 0.2em;
    border-radius: 6px;
}

mark {
    color: #CCCC44;
    font-weight: normal;
    background-color: navy;
}"
 :body
  `(,(img "/css/p/txtblog.png" "Textual Blogging With...")
    (h1 "TXTBLOG")
    (h2 "Finally... Plaintext Blogging!")
    
    (p "TXTBLOG is a UNIX utility that lets you easily transform your
    collection of plaintext (.txt) files into a well organized
    website!")

    (pre ,(m "Create some files:")"
$ touch 01-file.txt 02-file.txt 03-file.txt"
	 ,(m "Generate the blog:")"
$ txtblog ."
	 ,(m "index.html should be generated!")"
$ firefox index.html")

    (p "There's no weird functionality or cumbersome design. You get
    what you see. A script that generates a website for all your
    plaintext files. Take note that no actual conversion is performed
    on your textfiles. Textfiles are a great thing to have because
    they're the easiest kind of file to download. Rather, the gen-html
    program scans your directory for all text files, and points to the
    raw text files themselves from index.html. I prefer this, but many
      browsers make plaintext really small for some reason...")

    (p "You can also generate an Atom feed for your users who prefer to
    read their articles via feed reader:")

    (pre
     ,(m "Create some files:")"
$ touch 01-file.txt 02-file.txt 03-file.txt"
     ,(m "Generate the blog:")"
$ gen-atom ."
     ,(m "feed.xml should be generated!")"
$ cat feed.xml"
     ,(m "If you don't know Atom, you can ignore this."))

    (p "If you have any gripes with this software, let me know at
    'shack[AT]muto[DOT]ca'. Don't complain that there's already
    another software called 'TxtBlog'. I don't care.")
    
    (h2 Download)
    (pre "git clone https://gitlab.com/MutoShack/txt-blog.git")
    (p ,(a "https://gitlab.com/MutoShack/txt-blog"
	   "See the source code!" ))

    ,(img "/css/p/vine.png" "Thanks for visiting. :)")))
