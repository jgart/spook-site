;; The main index of the website.
(page
 :title "About Me"
 :body
 `((p "Hi, I'm Ben Card (MutoShack). I'm a functional programmer from
Alberta, Canada.")
   ,(img "/css/mutoface.png"
	 "A  picture of an extremely handsome man.")
   (p "I write code in "
      ,(a "https://lisp-lang.org/"
	  "Lisp")
      " and am very interested in POSIX-like systems, namely "
      ,(a "https://gnu.org/"
	  "GNU")
      ". My main computer runs GNU/"
      ,(a "https://kernel.org"
	  "Linux")
      ", but I often use a GNU/"
      ,(a "http://darnassus.sceen.net/~hurd-web/"
	  "Hurd")
      " in a virtual    machine.")
   (p "I enjoy writing in "
      ,(a "https://gnu.org/s/guile"
	  "Guile")
      " and "
      ,(a "http://www.open-std.org/JTC1/SC22/WG14/"
	  "C")
      ".")
   (p "I have a lifetime membership on Arpanet. "
      ,(a "http://mutoshack.jerq.org/index.html"
	  "See my SDF site")
      " or, if you have a "
      ,(a "https://wikipedia.com/wiki/Gopher_(protocol)"
	  "Gopher")
      "    client, "
      ,(a "gopher://sdf.org/1/users/mutoshack"
	  "browse my phlog")
      "!")

   (hr)
   (h2 (@ (id "contact")) "Contact Info")
   (ul
    (li "Email: "
	,(a "mailto:shack@muto.ca"
	    "shack [AT] muto [DOT] ca"))
    (li "XMPP: MutoShack@616.pub")
    (li "Mastodon: "
	,(a "https://functional.cafe/@mutoshack"
	    "functional.cafe/@MutoShack"))
    (li "Savannah: "
	,(a "https://savannah.gnu.org/users/muto"
	    "Muto"))
    (li "GitLab: "
	,(a "https://gitlab.com/MutoShack"
	    "MutoShack"))
    (li "Itch: "
	,(a "https://mutoshack.itch.io"
	    "MutoShack"))
    (li "My PGP public key is      "
	(pre "E0489FBE81FB3913BC77B39072CF10A241074882")))

   (hr)
   (h2 "Hobbies")
   (p ,(a "https://pixelfed.se/MutoShack"
	  "I'm a 35mm    photographer")
    ". I enjoy still-life and street photography and am always
learning new tricks in GIMP. Currently I'm thinking of investing in a
digital camera.")
   (p "I play the ocarina and I make an effort to sing regularly. I
also listen to music. I'm a fan of 'The Grateful Dead', 'Jukebox the
Ghost', 'Queen', and 'David Bowie'")
   (p "I keep a diary to record day-to-day life and poetic
expression.")
   (p "I study marketing and rhetorical tactics every now and then.")
   (p "I enjoy kendo, but I never joined a group.")

   (hr)
   (h2 "About this website")
   (p "This is my website, which I use to express thoughts too long to
fit in a Mastodon status. I'll write about computer programming most
of the time.")
   (p "This website follows the design philosophy that I describe "
      ,(a "/b/07-Good-Web-Design.html"
	       "here")
      ".")
   (p "I may introduce an advertising/sponsoring mechanism to this
site, but I don't want slow-as-heck .gif banners or Google
adsense. Give me some time to work out a fast & efficient system.")
   
   (hr)
   (h2 "My Goal")
   (p "I'm a total stdio junkie. I want to keep software small and
portable.")
   (p "Small is not the opposite of user-friendly. I believe small and
fast, well-documented programs are way better than a clusterdump of
bad animations and so-called \"eye candy\".")
   (p "Simply put... I want the world to stop moving backwards.")))
