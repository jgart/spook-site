;; A blog post talking about the coolness of SXML.
(page
 :title "Messing Around With SXML"
 :date "Jun 12, 2020"
 :body
 `((cc "There's a stone in my garden
With a key underneath
Make sure that you tell no-one
Take it and come with me.")
   
   (p "I've been interested in "
      ,(a "https://en.wikipedia.org/wiki/SXML"
          "SXML")
      " for a while. I just finished writing my own image "
      ,(a "https://gitlab.com/MutoShack/guimage"
          "gallery generator using SXML")
    ", and I have to admit... Guile doesn't exactly have amazing SXML
documentation, so I decided to write a bit about it.")

;;; WHAT'S SXML   
   (hr)
   (h2 "What's SXML?")
   (p "SXML, or \"Symbolic XML\" is a lispy, and dare I say, more
intuitive way of writing XML documents. For example, instead of
writing the following snippit in XML:")
   (pre
    "<bird>
 <type>Chickadee</type>
 <name>Joe</name>
</bird>")
   (p "You could simply write the following SXML expression:")
   (pre
    "(bird
 (type \"Chickadee\")
 (name \"Joe\"))")

   (p "If you take a look at the above snippits, you'll realize that
SXML is very much like XML. You just don't need explicit closing tags
for SXML. This comes naturally to Lisp and it would be nice if XML was
like this to begin with, but that's okay.")
   (p "One problem with SXML though, is the documentation. Every
implementation is a little different, and since it's not exactly a hot
topic, not a lot of people are writing about it.")
   (hr)
   (h2 "Getting Started")
   (p "I'm going to use the (sxml simple) module in Guile Scheme,
which comes with plenty of tools to help us get comfortable with
SXML.")
   (p "We'll start off by writing a \"Hello world\" XML document to a
file.")
   (pre
    "(use-modules (sxml simple)
 (let ((port (open-output-file \"test.xml\")))
  (sxml->xml '(p \"Hello SXML!\") port)
  (close-port port)))")
   (p "In this example we define \"port\", which is a file named
\"test.xml\". (sxml->xml) lets us specify the port we write to, so we
tell SXML to write to our file.")
   (p "test.xml should now caontain the line '<p>Hello
world!</p>'. You can also transform XML to SXML with '(xml->sxml)'.")
   (p "Because SXML is really just XML, all you need to know are the
oddities and differences of SXML. For example, attributes in SXML are
different:")
   (pre
    "(bird (@ (type \"Chickadee\")
	 (name \"Joe\")))
;; <bird type=\"Chickadee\" name=\"Joe\">")

   (p "honestly, I think it would have been better if attributes were
Guile tags instead, for example:")
   (pre
    ";; Incorrect code. This will not run
(bird
 #:type \"Chickadee\"
 #:name \"Joe\")")

   (p "I don't know how Guile tags work on a low level, though, so I
can't complain.")

;;; ABSTRACTIONS   
   (hr)
   (h2 "Abstractions")
   (p "Because Guile's SXML module is part of Guile, you can use the
power of Guile to make your life easier through abstractions!")
   (p "For example, You can use Scheme variables in SXML if you use a
backquote (`)")
   (pre
    "(define name \"Jimbo\")
(sxml->xml `(p \"Hey there, \" ,name))
;; <p>Hey there, Jimbo</p>")
   (p "Variables can come in handy when you want your user to be able
to customize a lot of things!")
   (pre
    "(define me '((name \"Spongebob\")
	     (title \"My Work Blog\")
	     (message \"Welcome to my website!\")
	     (best-friend \"Patrick\")))")
   (p "You could then use association lists for every sublist defined
in 'me'!")
   (p "I tend to use custom functions as shortcuts for myself, so I
don't need to write out loooong strings of SXML all the time:")
   (pre
    "(define (a link string)
 `((a (@ (href ,link)) ,string)))

;; Now instead of writing *this* all the time:
 (a (@ (href \"https://muto.ca\")) \"My site!\")

;;I can just slap down:
,(a \"https://muto.ca\" \"My site!\")")
   (p "I know I'm taking this a bit far, but I like abstraction, so
let's continue.")
   (p "You can create a post template for all your files, and create
new files based on it easily:")
   (pre

    "(define (post title date body)
 (let ((port (open-output-port \"post.html\")))
  (display
   (string-append
    \"<!DOCTYPE html>\\n\"
    \"<html>\\n\"
    \"<head>\\n\"
    \"<title>\" title \"</title>\\n\"
    \"</head>\\n\"
    \"<body>\\n\"
    \"<h1>\" title \"</h1>\\n\"
    \"<h2>\" date \"</h2>\\n\") port)
    (sxml->xml ,body port)
  (display
   (string-append
    \"</body>\\n\"
    \"</html>\") port)))")	      

   (p "We can use this function like so:")
   (pre
    "(post \"My Test Post\"
       \"May 14, 2020\"
 '((p \"Hello and thanks for checking out this blog post!\")))")

;;; CONCLUSION   
   (hr)
   (h2 "Conclusion")
   (p "I enjoy writing in SXML, although I wish it had a standard
design. Every implementation is a little different. For example,
Guile's SXML doesn't include support for <!-- comments -->, so I had
to modify the existing module just so I could have comments!")
   (p "I still need to get used to SXML. I really enjoy it and I feel
that, with enough time and the right abstractions, I could have a
blast with this little module!")
   (p "Anyway, since this post may leave you wanting more, Here's a
list of resources that could help you find your way in the mystical
world of SXML:")
   (ul
    (li ,(a "http://okmij.org/ftp/Scheme/SXML.html"
	    "SXML at okmij"))
    (li ,(a "https://web.archive.org/web/20070414181503/http://modis.ispras.ru/Lizorkin/sxml-tutorial.html"
	    "An SXML tutorial from modis.ispras.ru"))
    (li ,(a "https://en.wikipedia.org/wiki/SXML"
	    "SXML on Wikipedia"))
    (li ,(a "https://www.bytefish.de/pdf/xml_with_scheme.pdf"
	    "XML With Scheme"))
    (li ,(a "https://www.gnu.org/software/guile/manual/html_node/SXML.html"
	    "SXML in the Guile Reference Manual"))
    (li ,(a "http://ssax.sourceforge.net/"
	    "S-exp based XML parsing")))))
