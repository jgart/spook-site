(page
 :title "Get Spooked"
 :date "Jul 6, 2020"
 :body
 `((cc "Can you hear the distant wolves
Howl at the crescent moon?")
   
   (p "I've been writing a " (i "lot") " of SXML lately. It's pretty
cool! Over the past few days I've been hacking up a little static
website generator called "
      ,(a "/p/spook.html" "Spook")
      ". Earlier today I completely migrated my website from David's "
      ,(a "https://dthompson.us/projects/haunt"
	  "Haunt")
      " generator to Spook. I made website generators in the past, but
Spook is special to me, so I want to talk a bit about it.")

   (hr)
   (h2 "Simple Design")
   (p "Spook reads a directory of SXML files, and converts them to
HTML. Then it puts the HTML pages in a new directory that you can copy
to your webserver's root. Spook comes with the tools required to make
a blog listing, as well.")
   (p "Although I want Spook to be hackable, I want the \"hacking\"
part to be optional. At the time of writing I still need to perform
some interface programming in order to make Spook more intuitive, but
it's working well so far.")

   (hr)
   (h2 "Easy to Start")
   (p "Spook comes with a template that you can build on, minimizing
the amount of guesswork you need to do after downloading. Simply edit
the 'spook.scm' template file and the page files in the 'page/'
directory, and run 'main.scm' to generate your site in a new folder
named 'site/'!")
   (p "Spook uses Guile's SXML functionality for page creation. SXML
is essentially HTML, so if you know HTML, SXML will be very easy!")

   (hr)
   (h2 "Everything I Need")
   (p "I don't know about what YOU need in a blog generator, but it
serves it's purpose for me. Just take a look at this example page:")
   (pre "(page
 :title \"This is a Page!\"
 :date \"Jan 1, 1970\" ; Optional
 :style \"body {background: lightblue;}\" ; Optional
 :body
 `((p \"Hello, world!\")
   (p \"How are you today?\")))")
   (p "As you can see, DATE and STYLE can be ommitted entirely. You
only really need a date for blog posts, not for regular pages, and you
rarely ever need an inline stylesheet (since you can define an
external stylesheet in spook.scm)!")
   (p "Of course, sometimes you want to re-style a webpage. For
example, most of the items in my "
      ,(a "/p/" "Projects")
      " directory have custom stylesheets in them, for marketing
purposes.")

   (hr)
   (h2 "As Many Blogs As You Want")
   (p "With Spook, you can define multiple blog directories. For
example:")
   (pre "(blog
 :title \"First Blog\"
 :msg   \"Recent Posts\"
 :path  \"blog/\")

(blog
 :title \"Projects\"
 :msg   \"My Projects\"
 :path  \"projects/\")

(blog
 :title \"Random Things\"
 :path \"things/\")")

   (p "This will generate 'site/blog/index.html',
'site/projects/index.html', and 'site/things/index.html'.")
   (p "The 'msg' key is an H2 element above the list of
posts. \"Random Things\" does not specify a message and therefor Spook
will not include one in the index file.")
   (p "I think it's handy to be able to do this. For example, I want
to list out all my blog posts, and also list all my software projects,
so that's how I do it!")))
	
