(page
 :title "Why I Love My PS Vita in 2018"
 :date "Jun 29, 2018"
 :body
 `((cc "I screamed, in the hopes that an angel, or a demon, or a
human would hear me, but I did not make a sound.")

   ,(img "/css/vita/Vita1.jpg"
         "[Picture of the Playstation Vita]")
   (p "The Plastation Vita (which held the name \"NGP\" or \"Next
Generation Portable\" before the initial release) is my personal
favourite handheld gaming console and I don't believe it was a
complete failure in the market (it's a real shame Sony deemed it as
such). This is going to be a shorter article because I just wanted to
give the Vita some public love.")
   (p "I'm going to divide this into two parts (pros and cons), with a
bias, 20-word conclusion at the end. So, without further adieu, here
are some of the things I love about my Vita:")

;;; OPERATING SYSTEM   
   (hr)
   (h2 "The Operating System")
   (p "I'm usually a huge fan of free software, but I have a soft spot
for the Vita's default firmware. I mean seriously, the bubble icons
are adorable & kind of professional looking at the same time!")
   ,(img "/css/vita/system.gif"
         "[Screenshot of the Vita's home menu]")
   (p "I can't help but be impressed by this piece of software, it all
just feels so intuitive. It might just be me, but Vita OS is the best
mobile operating system I've ever used. Instead of swiping down from
the top or bottom of the screen (on iOS/Android) to access the \"Quick
Menu\" (used to turn Wifi or Bluetooth on/off and change brightness),
simply hold down the \"PS\" button. On Nintendo's 3DS you would need
to suspend the application and find the settings menu on the top-left
to change these settings!")
   (p "I also love the variety of colours and effects available for
backgrounds (you can add an image to the background if you prefer)")
   ,(img "/css/vita/background.gif"
         "[Screenshot displaying list of background colours]")
   (p "Another nice thing is that there's a way to see all your open
applications. In th home menu just press the PS button and you'll see
this neat, clean little list of all your open apps!  (I just wish
there was a way to remove those ads!)")
   ,(img "/css/vita/suspend.gif"
         "[Screenshot of suspended applications in a list]")

;;; REAR TOUCHPAD   
   (hr)
   (h2 "Rear Touchpad")
   (p "It took me a while to think about this one. Some people may see
the rear touchpad as a bad thing, but the idea is actually pretty
cool!")
   ,(img "/css/vita/rear.jpg"
         "[Picture of the back of the Vita. It has a rear touchpad]")
   (p "The PS Vita 2000 implements the rear touchpad better by adding
larger pads to rest your fingers on, but my 1000 still does a good
enough job, at least to the extent where it isn't uncomfortable.")
   (p "Not a lot of games use the rear touchpad for anything, but when
they do I feel like it's a pretty unique experience (there is an
option in Jet Set Radio to control the camera with the touchpad. In
Little Big Planet there are certain platforms that you cannot jump on
until you hit them with the touchpad. Many apps and games do small
things like this, I find it cute, fun & sometimes handy)")

;;; GAME LIBRARY   
   (hr)
   (h2 "Library")
   (p "The Vita isn't known for having a whole lot of games, but let
me just say... The Vita has a whole lot of games.")
   ,(img "/css/vita/library.jpg"
         "[A picture of a bunch of Vita games on a shelf]")
   (p "Now, I'm talking about good, real games. The Apple App Store,
Nintendo eShop & Sony's PS Store are basically software repositories
for specific machines. If I made my own video game console with my own
store on it and just sold ten billion \"Hello, World!\" programs, I
could gloat about having \"Ten *billion* games in the store!\", so
let's remove quantity from this section.")
   (p "We all know the very best Nintendo games are the ones Nintendo
designs and makes themselves, I believe it is the same way with
Sony. The issue here is that Sony gave up on the Vita very (very)
early on, so the only top-notch video games we have are the ones that
showcase the features of the system: \"Gravity Rush\", \"Tearaway\"
\"Little Big Planet\" and the like.")
   ,(img "/css/vita/lib2.gif"
         "[A picture of theVita's software library list]")
   (p "However, the good thing about the Vita, is that even though
Sony stopped producing official games for it, there are still some
amazing titles available (\"Hohokum\", \"Limbo\", \"Jet Set Radio HD\"
and \"Proteus\" to name a few). The Vita is doing extremely well even
without official support!.")

;;; BATTERY LIFE   
   (hr)
   (h2 "Battery Life")
   (p "I have heard some people say that they are not satisfied with
the Vita's battery life, but mine lasts all day when it isn't
connected to the Internet (most of the time I'm on airplane mode
because I'm not a huge online person). I also disabled notifications
and removed my \"Start on boot\" programs.  I don't think you need to
check for updates every time you boot your system")

;;; THE BAD STUFF   
   (hr)
   (h2 "The Cons")
   (p "As much as I love the Vita (a lot), there are some things that
I'm no fan of. Let's go over a few:")

   (hr)
   (h2 "Sony hates it")
   (p "Everyone loved the PSP, it was a huge hit, so I could only
imagine that they wanted to create a system just as successful as the
PSP. The problem, as I see it, is that they wanted the Vita's success
to be instantaneous! When the Vita didn't explode & kill the 3DS in
the blink of an eye, they decided \"welp! Must be because no-one likes
it!\", turned their heels and walked away. If anyone has a better
reason for the Vita's failure, please contact me. In fact, Sony, you
*still* can make the Vita a success if you try! By today's standards
it's still a brilliant handheld device, it's not like the Vita is the
\"Dashcon\" or \"Fyre Festival\" of game consoles, right?")
   
   (hr)
   (h2 "Lack of colours")
   (p "The Vita was available in black. That's it (unless you get the
special edition white, or order a different colour from Japan). This
is kind of important to me, even though I *do* love my black Vita, if
I had a choice of what colour I wanted it would be a more fun
experience to buy one! The Vita 2000 had a blue version here in North
America/Canada but I don't remember seeing any of the other colours.")
   ,(img "/css/vita/colour.jpg"
         "[Picture of a bunch of Vitas, all in different colours]")
   (p "(Image credit goes to whoever made it. I love you, don't sue
me.)")
   
   (hr)
   (h2 "Glossy finish")
   (p "The 2000 did a better job on this, but the rear touchpad was
still glossy. I understand the screen digitizer, but the rear pad
doesn't have to be. No-one likes fingerprints.")
   
   (hr)
   (h2 "The Camera")
   (p "I wouldn't call this a con, in fact I don't find cameras on a
game console that helpful at all, but the Vita has one (and some games
make good use of it). Here's a few example photos I took in a nearby
forest:")
   (p "Looking up at a tree")
   ,(img "/css/vita/vitatree.jpg"
         "[Photo taken with the vita, of a nice tree]")
   (p "I call this one \"Athletic Man\"")
   ,(img "/css/vita/vitaball.jpg"
         "[Photo taken with the Vita, of a man running, about to kick
a soccor ball]")
   (p "This one has some direct sunlight in it")
   ,(img "/css/vita/vitaart.jpg"
         "[Photo taken with the Vita, of a stone pathway, with the
evening sun just above it]")
   (p "This should probably be an article all on it's own, but I want
to say that low resolution cameras function just fine for artistic
expression. Photography as an art, in my opinion, is about telling
stories & expressing emotion. The resolution of your camera doesn't
always matter so long as you can portray that emotion in a photo (in
fact, when I get my Mediagoblin set up, I want to have a collection
dedicated to \"Vita Photography\". Whether or not public registration
will be enabled is up to me & how much space my hard disk has.)")
   
   (hr)
   (h2 "Memory cards")
   (p "I almost forgot to mention this! Sony sold special proprietary
memory cards for the Vita & they cost a fortune! I found a 64 GB card
from a nice man who charged me $85 (CAD) for it, and that's considered
a *good deal* for these things! Of course you can install custom
firmware and buy a MicroSD adapter, but that means you'll have to
fiddle with the software. I'm a software fiddler by profession so when
the day is over, all I want to do is play a video game, not debug
*more* crap! The Vita 3000 needs MicroSD support, not proprietary
cards.")
   ,(img "/css/vita/cards.jpg"
         "[Picture of some tiny memory cards]")
   
   (hr)
   (h2 "Conclusion")
   (p "I love my Vita and you should love yours and I'm going to have
a tea and go to bed.")
   ,(img "/css/vita/bottom.jpg"
         "[Picture the Vita, laying on a nightstand]")))
