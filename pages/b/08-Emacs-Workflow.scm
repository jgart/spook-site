(page
 :title "Emacs Workflow Optimization"
 :date "May 26, 2019"
 :body
 `((cc "Aren't we all beautiful?
Why am I not beautiful?")
   
   (p ,(a "https://gnu.org/software/emacs"
          "Emacs")
    " is a text editor that does everything. On one hand I'm not the
biggest fan, since it's huge and doesn't really follow the "
    ,(a "https://en.wikipedia.org/wiki/Unix_philosophy"
        "UNIX philosophy")
    ". On the other hand, I've optimized my entire workflow around
Emacs, and love it too much to quit. Besides, it's nowhere near as
heavy & bloated as "
    ,(a "https://mozilla.org/en-US/firefox/new"
        "modern Web browsers")
    " or "
    ,(a "https://electronjs.org"
        "Electron")
    " programs!")
   (p "I want to talk about how I use Emacs daily, and how I optimize
Emacs to be faster for me.")

;; EMACS --DAEMON
   (hr)
   (h2 "Speeding things up with '" (i "emacs --daemon") "'")
   (p "Emacs takes a long time to start up (almost a second). Now when
my computer boots I have it run the command '" (b "emacs --daemon")
"', and from then on I can run the command '" (b "emacsclient -c") "'
any time. This starts Emacs instantaneously! The Daemon keeps an
instance of Emacs running in the background, but since I run Emacs so
often, it's nice to be able to start it so fast (it doesn't take up
much memory, anyway!)")
   (p "Since '" (b "emacsclient -c") " is long to type, I add an alias
for it in my .bashrc")
   (pre
"alias em=\"emacsclient -c\" #New emacs acts as emacsclient -c!"    

(p "I also byte-compile my ~/.emacs file with '") (b "M-x
byte-compile file RET ~/.emacs") "', which compiles .emacs into
.emacs.elc (elc is short for Emacs-Lisp-Compiled). Emacs automatically
uses the compiled file instead of the regular one, which makes the
initial startup faster!")
   
   (p "Since I use the "
    ,(a "https://i3wm.org" "i3")
    " tiling window manager, I made some keybinding shortcuts for
different Emacs functions:")
   (pre
"bindsym $mod+t  exec emacsclient -a "" -c
bindsym $mod+F1 exec emacsclient -a "" -e '(ibuffer)'    -c
bindsym $mod+F2 exec emacsclient -a "" -e '(rmail)'      -c
bindsym $mod+F3 exec emacsclient -a "" -e '(dired "~/")' -c
bindsym $mod+F4 exec emacsclient -a "" -e '(eshell)'     -c
bindsym $mod+F4 exec emacsclient -a "" -e '(eww "")'     -c")

   (p (b "$mod") " is bound to the " (b "Super") " key (some people
call it the \"Windows\" key), so if I just want to boot up my computer
and read my mail, I press '" (b "Super+F2") "' and it opens my Emacs
mail reader!")
   
   (p "Since Emacs is always running in the background, if I quit
emacs with '" (b "C-x C-c") "', my buffers will still exist. If I'm
working all day and close Emacs, I can press '" (b "Super + F1") "' to
open my buffer list, which will still display all of my open
files. Very handy! (If I want to \"completely kill\" emacs and all the
buffers, I run '" (b "M-x kill-emacs") "')")

;;;    
   (hr)
   (h2 "The same shortcuts, but from within Emacs")
   (p "Although I use the "
    ,(a "https://www.kaufmann.no/roland/dvorak"
        "Programmer Dvorak")
    "keyboard layout, I never actually changed my Emacs keybindings.
I'd say the default bindings work just as well on both QWERTY & Dvorak
keyboards.")
   (p "I did bind some of my favourite functions to the Fx keys,
though. If I currently have an Emacs window open, I don't need to
press '" (b "Super+F2") "' to read my mail, I can just press '" (b
"F2") "' from inside Emacs!")
   (p "My current bindings look like this in my .emacs file:")

   (pre
    ";;Emacs has trouble binding dired,
;;so I make a function call dired.
(defun my-dired ()
 (interactive)
  (eval-expression '(dired \"~/\")))

(global-set-key (kbd \"<f1>\") 'ibuffer)
(global-set-key (kbd \"<f2>\") 'rmail)
(global-set-key (kbd \"<f3>\") 'my-dired)
(global-set-key (kbd \"<f4>\") 'eshell)
(global-set-key (kbd \"<f5>\") 'eww)
(global-set-key (kbd \"<f6>\") 'run-guile) ;for geiser
(global-set-key (kbd \"<f7>\") 'gnus)
(global-set-key (kbd \"<f8>\") 'info)
(global-set-key [(shift f1)] 'byte-compile-file)
(global-set-key [(shift f2)] 'kill-emacs)
(global-set-key [(shift f3)] 'indent-region)
(global-set-key [(shift f4)] 'fill-region)
(global-set-key (\"C-<backspace>\") 'whitespace-cleanup)")

   (p "As you can see, my in-Emacs and out-of-Emacs bindings are
similar, " (b "F2") " and " (b "Super+F2") " both open rmail to avoid
confusing myself. " (b "Shift+fx") " keys are for functions I use
often and are handy to have as hotkeys. " (b "C-<backspace>") " gets
rid of excess whitespaces at the end of lines (because " (b
"M-<backspace>") " is how I delete full words.)")

;;; RMAIL   
   (hr)
   (h2 "Reading email with " (i "rmail"))
   (p "Rmail is the Emacs email reader. A good introduction can be
found by running '" (b "info emacs rmail") "' at the terminal. I'm
just going to talk about how I set my mail up (since, admittedly, I
had some trouble setting my mail up)")
   (p "To send mail I use "
    ,(a "https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol"
        "SMTP")
    ".  To receive email I use "
    ,(a "https://en.wikipedia.org/wiki/Internet_Message_Access_Protocol"
     "IMAP")
    ". Emacs works with a program called "
    ,(a "http://mailutils.org/manual/html_node/movemail.html#SEC109"
        "Movemail")
    "to do stuff. The Movemail command that comes bundled with Emacs
doesn't support IMAP, so you may have to install the '" (b
"mailutils") "' package on your own package manager.")
   (p "Here's what my email config looks like in my .emacs:")
   (pre
    "(setq
  send-mail-function 'smtpmail-send-it

  rmail-primary-inbox-list
  (list
   (concat
    \"imaps://mymail%40example.com:\"
    \"MyPassw0rd\"
    \"@mail.example.com\"))

  rmail-preserve-inbox t
  rmail-file-name \"~/.RMAIL\"
  user-mail-address \"mymail@example.com\"
  user-full-name \"Muto\")")
   (p "The '" (b "rmail-primary-inbox-list") "' variable specifies
where my remote mailbox is:")
   (ul
    (li (b "imaps://") " tells us we are using the IMAPS
protocol. This can be replaced with " (b "pop://") " or " (b
"pops://"))
    (li (b "mymail%40example.com") " is our mail address.  "
     ,(a "https://tools.ietf.org/html/rfc3986#page-12"
         "%40")
     " is the standard URI way to type the '" (b "@") "' symbol
(corresponding to ASCII Unicode U+0040) ")
    (li (b "MyPassw0rd") " is our password. The username & password
are separated by a colon.")
    (li (b "mail.example.com:993") " is the hostname we're getting our
mail from, followed by the port number (By convention, IMAPS is
usually on port 993, but I think this can be different)"))

   (p (b "rmail-preserve-inbox") ". By default, even in IMAP, Emacs
will download your emails, then delete all the mail from the
server. To stop this, you can set rmail-preserve-inbox to true. The
problem is Emacs will download duplicates if you reload it twice. The
best way to deal with this is to have a main computer that downloads
email, and use rmail-preserve-inbox on your secondary devices.")
   (p "The '" (b "rmail-file-name") "' variable specifies where to
store our mail (the default is \"RMAIL\", but I like it to be hidden,
so I specify \".RMAIL\")")
   (p (b "user-mail-address") " specifies what your email address
should be printed as when you're sending an email to someone.")
   (p "After all that it becomes easy to invoke '" (b "M-x rmail") "',
which will collect all my mail from my remote inbox. Very nice!")
   
   (hr)
   (h2 "Web Browsing With " (i "eww"))
   (p "Eww is the Emacs Web browser. Eww browses The Good Web (\"The
Good Web\", as I call it, is the Web without JavaScript or CSS)!  I
use this browser regularly in Emacs to quickly find some information
(it works very well with Wiki sites and simple blogs). The Eww
documentation says it handles 'basic CSS', but I don't believe it
does. Emacs can also be compiled with WebKit to provide a more
fully-functional browser, but I personally like the smallness of Eww
much more.")
   (p "Start eww with '" (b "M-x eww") "' and you'll be prompted to
make a websearch (by default, eww uses DuckDuckGo for search
results. There are a few handy features of eww that I use
regularly:)")
   (ul
    (li (b "g") " reloads the page.")
    (li
     (b "R") " switches to \"Readable mode\", which can be extremely
handy, especially for websites that don't support text-based browsers!
It determines which part of the page is \"readable\" content, and only
displays that. It doesn't always work, though.")
    (li (b "b") " adds a page to your bookmarks, and " (b "B") " to
view them."))
   (p "Unfortunately, eww does not support "
    ,(a "http://gopher.floodgap.com/gopher/"
     "gopher://")
    " URLs. For that, I'd suggest "
    ,(a "https://lynx.browser.org/"
     "Lynx"))
   (p "This blog is text browser compatible, and I encourage you to
read it with " (b "M-x eww") ".")

;;; CONCLUSION   
   (hr)
   (h2 "Conclusion")
   (p "I'm going to keep this article short. I won't add sections
about dired or gnus, as they're well documented in the Emacs manual. I
have a lot of things I want to write about on this website, but
everything in my oven is barely half-baked. Maybe I'll read a book on
how to write " (i "interesting") " articles.")))
