(page
 :title "All About Plaintext"
 :date "Mar 19, 2020"
 :body
 `((cc "Reluctant, waiting, and anxiously restless,
He draws pictures of things he fears
\"If life's  worthless why's the world so against us?\"
He was alone when he broke into tears.")
   
   (p "In the midst of the "
    ,(a "https://en.wikipedia.org/wiki/Coronavirus_disease_2019"
     "Covid19")
    "pandemic, I'm trying not to die of boredom while stuck inside, so
I've been writing a whole lot of articles in plaintext lately, whether
it be technical musings on my "
    ,(a "http://mutoshack.jerq.org/log"
     "TXT-BLOG site on SDF")
    ", or a bunch of random stuff on my gopherhole, or just journal
files on my computer. I've taken an interest in the beautiful
simplicity of "
    ,(a "https://en.wikipedia.org/wiki/Plain_text"
     "plaintext")
    ", so I'm taking the time to talk about it here.")

;;; PLAINTEXT IS BEAUTIFUL   
   (hr)
   (h2 "Text is the best thing ever")
   (p "Nothing can really rival the beauty of an old fashioned, "
    ,(a "https://en.wikipedia.org/wiki/UTF-8"
     "UTF-8")
    "encoded, UNIX format plaintext file. A simple text file is
readable from just about any machine in the world, from ATM's to
supercomputers to smartwatches to fridges. You can't read a Word
document or an HTML page on a smartfridge. Well, you *can* but it'll
most definitely take a lot more work than reading plaintext!")
   (p "To be honest, plaintext is the only text format people can't
royally screw up. Sure, some jocks try to make big fancy ascii borders
around their articles, some people don't care to follow the "
    ,(a "https://www.emacswiki.org/emacs/EightyColumnRule"
     "80-column    rule")
    ", some people write in weird non-UTF-8 character encodings, some
people use ð\x9d\x94\xa0ð\x9d\x95£ð\x9d\x95¬ð\x9d\x96¹ð\x9d\x9b¾
characters all the time, just because they think it's cool (it is
not), some people use lots and lots (and lots) of spacing between
paragraphs (we're talking like, 5 newline characters), but nearly
everything bad about a \"bad\" textfile can *usually* be reformatted
in a matter of simple shell scripts! Generating plaintext from HTML is
a bit of work, but using a regular expression to get rid of excessive
newlines in a plaintext file is super simple! In fact, it's a sed
one-liner:")
   (pre "# Remove all gaps of 4 empty lines:
sed -z 's/\\n\\n\\n\\n//g' oldfile.txt > newfile.txt")
   (p "And on top of all that, it's not nearly as bad as what people
do with HTML, or Markdown, or Skribe, or Word, or Latex.  People like
to use BIG FONTS, or tiny fonts, or weird background colours. Some
people like to write in someWordEditor you never heard of, that uses a
proprietary file format, forcing *you* to download yet another program
just to read your stupid co-worker's documents! Some people like to
use somePopularWordEditor you *have* heard of, but it doesn't work on
Unix, so you need to start a display server and download some piece of
software that haff-assedly retrofits the darned file format for your
machine!")

;;; TEXT HAS NO REASON TO DIE   
   (hr)
   (h2 "Text has no reason to die")
   (p "Plaintext is timeless. It will never become obsolete, or be
replaced, because there's no reason for it to become obsolete, or be
replaced! You can read a plaintext file from 1970 on a computer in the
year 2050, because plaintext is a perfect format!")
   (p "Plaintext files are small. A 2,000-character TXT file is, well,
2KB. A Word document of the same size might take up 10KB (I don't have
Word on me right now, so this is just an estimate). In fact, the
entire text of the book \"Dracula\" is less than a single megabyte of
space! You can fit a whole Dracula and a half on a floppy disk. Text
is small!")
   (p "Also, I should mention (and this is the coolest thing in the
world)... UTF-8 is backwards compatible with ASCII, and it
accomplishes that without being held back by *any* downsides of
backwards compatibility. It just came naturally. It just *is*.  It's
insane.")

;;; THE END   
   (hr)
   (h2 "The End")
   (p "That's it. That's all I really wanted to rant about today. I
feel like we spend so much time working on an unnecessary level of
abstraction that we forget how awesome and totally viable plaintext
is! I mean (and I say this too often), why on earth do so many people
send email in HTML? That strikes about a 0 on my sanity-O-meter.")
   (p "Anyway. Wash your hands, write in plaintext, check out my "
    ,(a "https://gitlab.com/MutoShack/txt-blog"
     "Plaintext    blogging generator")
    ", and stay safe!")))
