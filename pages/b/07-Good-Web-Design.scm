(page
 :title "Web Design Practices that Don't Suck"
 :date "Apr 01, 2019"
 :body
 `((cc "At night outside my window sill
The air was moist and perfectly still
And as I looked up at the eerie dark sky,
A distant blinking red light caught my eye.")

   (p "(This is NOT an April Fool's Day joke!)")

   (p "A few months ago I tried writing a post, the title of which
was: \"Web Design Practices that " (i "do") " suck\". I typed for
days and days about the awful things people do to their websites. The
more I wrote, the more I asked myself \"" (i "why?  Why do people do
such stupid things?")  "\" Eventually I got too frustrated and smashed
my computer.")
   
   (p "Moral of the story? Millions of computers die every year
because of bad web design.")
   (p "This time around, however, I'm going to try focusing on " (i
"good") " web design tips, things that make me feel warm and fuzzy
inside! See, it's really easy to make a good website, and I'm going to
talk about how (of course, I'll talk a *little* bit about the things I
hate.)")
   (hr)
   (h2 "Make Your Site Comfortable in Pure HTML")
   (p "In the world of CSS and JavaScript, it's hard to remember the
importance of HTML. The most important part of any website is the
content inside, and usually that content is in HTML.")
   (p "Many people don't care about CSS at all, and opt to use
text-based browsers like "
    ,(a "https://lynx.browser.org/"
        "Lynx")
    ", "
    ,(a
      "http://w3m.sourceforge.net/"
      "W3M")
    " or "
    ,(a
      "https://www.gnu.org/software/emacs/manual/html_mono/eww.html"
     "Eww")
    ".  This type of browsing can actually be really efficient. In
fact, it would be "
    (i "more")
    " efficient than GUI browsing if    websites didn't "
    (i "require")
    " us to enable JavaScript just    to load their site!")
   (p "It might feel like a pretty restricting goal, but I want you to
make text-based Web browsers a \"top priority\" in your design, with
CSS/JS being an optional addition. As an example, here's what this
website (muto.ca) looks like under Lynx:")
   ,(img "/css/webdes/lynx.png"
	 "A text-only browser reading muto.ca")
   (p "This website is very clean in a terminal. Yes, thank you.")
   (hr)
   (h2 "Simple CSS")
   (p "A lot of websites overuse CSS, with 500+ rules, even! When you
write a website, keep it simple. You're not putting on a performance,
and you're not showing off your \"sick skills\".  You're uploading
information that people want to access. In fact, this is probably my
main point of the whole article: Your site isn't " (i "for") " your
site. It's for your viewer to get information. If info is not the
first thing your user gets, you're doing it wrong.")
   (p "CSS is what makes your site unique, but remember that your site
has one purpose: for users to gain (or send) information.  Failing to
accomplish this is failing to build a website.")
   (p "Write like someone else will read it. You want them to " (i
"enjoy") " reading your CSS code (preferably in one sitting)! This
mindset is what turned me into a perfectionist with my code. In fact,
I recently switched from using "
    ,(a "https://en.wikipedia.org/wiki/Hexadecimal"
        "Hexadecimal color values")
    " to "
    ,(a "https://en.wikipedia.org/wiki/X11_color_names"
        "X11 Color names")
    ". This is not something I particularly " (i "recommend") " doing,
per se, but \"Midnight Blue\" is easier to understand for the human
reader than \"#191970\". This is a personal choice, as I like natural
language over numerical values, but name your colours however you
want!")
   (p "Oh yeah, and remember to stay away from \"display: sticky\".
This one, in particular, is used to make a certain object \"stick\" to
the screen, no matter where you scroll. This is annoying and usually
useless. To make matters worse, slower machines have a really hard
time with sticky stuff!")
   (p "You can "
    ,(a "https://muto.ca/css/style.css"
        "view the CSS file of this website")
    " if you want.")
   
   (hr)
   (h2 "JavaScript...")
   (p "The Web is too slow to animate every bit of your site.")
   (p "There's a lot of pressure, for some reason, to build your site
with lots of JavaScript animations in it. I don't know why this
is. When I hover over a button that slowly fades to a different
colour, it makes my brain think \"oh, I'll wait until the button is
'ready' to be pressed\". This is bad, in case you haven't already
gotten that.")
   (p "Your website should function just fine without JavaScript.
Personally I find any kind of animation to be unneeded complexity, but
if you " (i "must") " have animations, then use vanilla JS (don't use
a framework like JQuery just to make a short animation!)")
   (p "The Web is slow, really slow. Don't use much JS at all (I'd
advise you to simply *never* use JS, actually.) It takes time to load
in the Javascript to your client's machine, and if you decide to write
something that takes readability away from your site, don't add it!
(For instance, don't make text \"fade in\" while you scroll through
the page. This does not make your site look \"smoother\", it makes you
look like a kid who just discovered how to use JavaScript 15 minutes
ago.)")
   
   (hr)
   (h2 "Use Local Fonts")
   (p "Remember: The most beautiful font is also the most boring
font.")
   (p "If someone visits your website, the first word that comes to
mind should be \"words\", not \"font\". the font that most people are
accustom to is " (i "the one that's already built into their
computer") "! Many web development books and tutorials really push the
idea of using " (i "external fonts") " from "
    ,(a "https://fonts.google.com"
     "fonts.google.com")
    " or "
    ,(a "https://www.fontsquirrel.com/"
     "FontSquirrel.com")
    ", but I would strongly advise you to " (i "not") " do this! It's
a common mistake. Loading a font from Google Fonts means that your
viewer's computer will have to access your website, pull out a piece
of data that says \"oh the font we want you to use isn't here, go to
Google for that\", then access " (i "another") " site just to grab the
font you want! It's a " (i "font") "! A much better way of doing this
is by simply writing something like this in your CSS file:")
   (pre
    "body {
 font-family: \"DejaVu Sans\", Ariel, FreeSans, sans-serif; }")
   (p "This makes " (i "way") " more sense than relying on an external
server. Everyone has at least one font on their computer, and it's
probably the one they're most used to, so let them use it!")
   
   (hr)
   (h2 "Choose a light framework, if any")
   (p "Although many website authors can easily get away with just
writing plain HTML and putting it on the Web, sometimes it's nice to
have a bit of automation done for you (for instance, if you run a blog
like this one, you don't want to update the \"recent posts\" page,
along with the atom.xml file manually).  This is where a \"static
website generator\" comes in handy. A static site generator builds
your website locally on your machine, so all you have to do is push
them to your Web server as plain HTML (in other words, it's still
plain HTML, it was just generated on your computer, so your site is
still just as fast as it would be if you manually wrote it all
down!)")
   (p "Some handy website generators are listed here:")
   (ul
    (li ,(a "https://dthompson.us/projects/haunt.html"
	    "Haunt")
	" - Scheme ")
    (li ,(a "/p/spook.html"
	    "Spook")
	" - Scheme ")
    (li ,(a "http://jmac.org/plerd/"
	    "Perld")
	" - Perl ")
    (li ,(a "http://luapress.org/"
	    "Luapress")
	" - Lua ")
    (li ,(a "https://blogc.rgm.io/"
	    "BlogC")
	" - C ")
    (li ,(a "https://www.getzola.org/"
	    "Zola")
	" - Rust ")
    (li ,(a "https://sculpin.io/"
	    "Sculpin")
	" - PHP ")
    (li ,(a "https://dalgona.github.io/Serum/"
	    "Serum")
	" - Elixir "))
   (p "Mind you, I did not test any of the other ones on the list, so
I don't know which ones are good or bad.")
   (p "This site (muto.ca) was generated with Spook, and you can find
the source code for this site "
    ,(a "https://gitlab.com/mutoshack/spook-site/"
        "here")
    "!")

   (hr)
   (h2 "Miscellaneous stuff")
   (p "Just so I get it off my chest, here's a list of pointers:")
   (ul
    (li "HTML first")
    (li "Simple CSS rules")
    (li "Light JS, if any at all")
    (li "No pictures over 1Mb (I'd prefer <500Kb)")
    (li "No video over 1080p (I'd upload 420p)")
    (li "Nothing ruled as - display: sticky")
    (li "No \"share/like\" buttons (we know how to copy a URL)")
    (li "Don't make things that are constantly moving")
    (li "Try not to mess too much with the defaults")
    (li "Contrast between font & background for readability")
    (li "Fit only what's relevant on the page")
    (li "Do not rely on external servers")
    (li "Don't use a CSS/JS library for small things"))
   (hr)
   (h2 "Other Writings On Web Design")
   (p "Here are a few articles that follow this style of web design:")
   (ul
    (li ,(a "http://www.catb.org/~esr/html-hell.html"
            "HTML Hell")
     " - An article about the hellish HTML some people write.")
    (li ,(a "https://simpleweb.iscute.ovh"
            "Simple Web is Cute")
     " - A tiny page describing good web design philosophy.  ")
    (li ,(a "https://pxlnv.com/blog/bullshit-web/"
           "The Bulls**t Web")
     " - An article describing the worst of the worst design
practices.  ")
    (li ,(a "http://webpagesthatsuck.com"
            "Webpages that Suck")
     " - A webpage that sucks talking about webpages that suck."))

   (p "Likewise, here are a few examples of " (i "bad") " web design,
some of which are ironically " (i "about") " web design:")
   (ul
    (li ,(a "https://www.thebestdesigns.com/"
            "The \"Best\" Designs")
     " - A twisted concept of \"Best\".")
    (li ,(a "http://www.goodweb.design/"
            "\"Good\" Web Design")
     "- The more I look at sites like this, the less I like
JavaScript.")
    (li ,(a "https://www.smashingmagazine.com/"
            "Smashing Magazine")
     " - Definitely Smashing... My hope in humanity.")
    (li ,(a "https://www.awwwards.com/"
            "Awwwards")
     " - Awards given out to 11-year-olds who still make sites in
Bootstrap.  ")
    (li
     ,(a "https://www.cnn.com/"
      "Just ")
     ,(a "https://www.huffpost.com/"
      "about ")
     (a "https://abcnews.go.com/"
      "every ")
     ,(a "https://www.theguardian.com/international"
      "major ")
     ,(a "https://www.nytimes.com/"
      "news ")
     ,(a "https://www.nbcnews.com/"
      "website ")
     ,(a "https://www.foxnews.com/"
"ever")))
(p ". Seriously. The hardest thing to find on news sites is the news
itself.  ")
   (p
    "That's it for this article. There are a lot of things I've    been meaning to write about, but I've been lazy lately. This    was fun and relaxing to put together. I hope it helps    someone.")))
