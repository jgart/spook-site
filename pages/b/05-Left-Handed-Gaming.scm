(page
 :title "Gaming with a Left Handed Setup"
 :date "Jul 17, 2018"
 :body
 `((cc "The masses came flooding in,
Widows, children thieves, and men.
They killed the sick and raped the land,
And worshiped Gods that they called \"brands\".")

   (p "In memory of 2018's failure that was the "
      ,(a "https://www.kickstarter.com/projects/razer/razer-naga-trinity-left-handed-edition"
	  "Left-handed Naga Trinity")
      " campaign, I'd like to talk about left handed gaming in the
modern age.")
   
   (hr)
   (h2 "How I play games left handed")
   ,(img "/css/lefty/ZDeskAll.jpg"
	 "[Picture of a standing desk next to a window]")
   (p "Back when computer mice were fairly new, left-handed mouse
users weren't extremely uncommon, in fact, just about every mouse was
ambidextrous. Fast forward a generation, everyone (even most left
handed people) use a computer mice right handed - it's
conventional. I'm totally okay with that, but personally I use my
mouse left handed due to habit & practical reasons (who wants to reach
all the way over their numpad to grab the mouse?)")
   (p "Specifically for this article, I thought I'd talk a bit about
my experience with left handed gaming. I'll give you a list of my
computer peripherals later, but first, keybindings:")
   (p "Some left handed gamers are able to play with "
      (b "WASD")
      ", while others use the arrow keys.    Personally I mirror the WASD convention by using \"P, L, Colon,    and Quote\" ("
      (b "PL;'")
      ").")
   ,(img "/css/lefty/Symbolics.jpg"
	 "A keyboard with PL:' highlighted")
   (p "A conventional lefty keybinding many games use is "
      (b "IJKL")
      ", probably because it's easier to pronounce than "
      (b "PL;'"))
   (p "When I watch right handed players use WASD, their pinky finger
naturally rests on \""
      (b "SHIFT")
      "\". Either it's just me & my distorted hands or there's something
about the other side of the keyboard, but my pinky finger rests on \""
      (b "RETURN")
      "\", so naturally, whenever something is mapped to Shift in a
right handed computer game, I remap it to Return (Video games should
be as comfortable as possible, since they really only exist to reduce
stress)")
   (p "I'll stop here about keybindings. I've been playing \""
      (i "Counter Strike: Global Offensive")
      "\" recently. I have never played FPS games before but it really
is quite fun (not a fan of the \"kill people\" aspect though, but the
strategy planning is fun). I might end up putting my CS:GO left handed
keybindings here later on, but not today (I'm tired and lazy)")
   
   (hr)
   (h2 "Hardware peripherals")
   (p "Anyway, I guess I'll write a bit about my hardware setup (for
some reason people I talk to want me to talk about hardware, so I
guess that's the popular thing)")
   (p "Some of this stuff is by Razer, but I strongly recommend "
      (b "NOT")
      " buying from Razer, as I found their products to be really low in
quality and durability. The only thing Razer does right is
marketing. And I mean, they "
      (i "really")
      " do their marketing right!")
   (ul
    (li "Mouse - "
	,(a "https://www2.razer.com/au-en/gaming-mice/razer-diamondback"
	    "Razer Diamondback"))
    (li "Keyboard - "
	,(a "http://matias.ca/ergopro/pc/"
	    "Matias Ergo Pro"))
    (li "Mouse pad - "
	,(a "https://www.pcgamingrace.com/products/glorious-extended-gaming-mouse-pad"
	    "Glorious Extended"))
    (li "Headphones "
	,(a "https://www2.razerzone.com/au-en/gaming-audio/razer-kraken-pro"
	    "Razer Kraken Pro")))
   
   (hr)
   (h2 "Mouse - Razer Diamondback")
   ,(img "/css/lefty/MouseAngle.jpg"
	 "A Razer computer mouse.")
   (p "I got this mouse on sale for $30 one day. For an ambidextrous
mouse it's pretty good and works well, but I find that I often press
the buttons on the left side of the mouse with my pinky or ring
fingers, which is a bit annoying sometimes. I feel like it's not well
made. I have this horrible scrollwheel problem where, when I scroll
up, it'll jump around and sometimes scroll more down than up. It's
hard to get ergonomics with an ambidextrous mouse, which is why I was
interested in the LH Naga, even though it "
      (i "is")
      "Razer.")
   (p "If you're looking for a good ambidextrous mouse, I'd direct you
towards the "
      ,(a "https://zowie.benq.com/en/product/mouse/fk/fk1.html"
	  "FK1")
      ", which I've heard everyone loves.")

   (hr)
   (h2 "Keyboard - Matias Ergo Pro")
   ,(img "/css/lefty/KeyboardMatias.jpg"
	 "A split keyboard")
   (p "This keyboard is amazing! It's almost as quiet as a membrane
but it's a mechanical switch. The comfort while typing on this thing
is just fantastic. I use it for all my work and gaming.  It has feet
that allow for negative tilt and tenting.")

   (hr)
   (h2 "Mouse pad - Glorious Extended")
   ,(img "/css/lefty/MouseMat.jpg"
	 "A white mousepad.")
   (p "I ordered this mouse pad one day and it's very nice. I also got
this wrist rest which is also nice")
   ,(img "/css/lefty/WristPad.jpg"
	 "A black wrist pad for a computer mouse")

   (hr)
   (h2 "Headphones - Razer Kraken Pro")
   ,(img "/css/lefty/Kraken.jpg"
	 "Some black headphones with a green accent")
   (p "These are the best headphones I have ever used, the only
problem is the in-line remote (inline remotes are a breakage point and
will wear down any pair of headphones faster). I'm thinking of just
cutting the remote off altogether & resoldering the strands, but not
until the sound quality is unbearable.")
   (p "If you're looking to buy a pair of headphones, I'd honestly
direct you towards the "
      ,(a "https://www.hyperxgaming.com/us/headsets/cloud-alpha-pro-gaming-headset"
	  "HyperX Cloud Alpha")
      ". It seems to be built from better materials and the cable is
detachable. If I didn't have my Kraken I would take the Cloud Alpha
any day.")
   (p "That's it for this article. If you want to send me feedback,
talk about your gaming setup, or simply give me a \"lol k\", email me
(shack AT muto DOT ca). Other contact methods are available on my
homepage. Until next time, take this midnight photo of a lonely
plastic rose.")
   ,(img "/css/lefty/Rose.jpg"
	 "A moonlit plastic rose on a windowsill")
   (p "That's it for this article. I'm sorry.")))
