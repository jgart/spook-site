(page
 :title "Customizing Emacs"
 :date "Feb 28, 2018"
 :body
 `((cc "Out of all my articles, this one scores highest on search
engines. It's annoying.

I commend joy, for there is nothing better for Man under the sun
than to eat, drink, and play Beetle Adventure Racing on the
Nintendo 64.")

   (p "It is now the year 2020 and I do not stand by this article
anymore. Do not waste your time reading it. I only keep it here to
avoid broken links on other sites.")
   (p "By that I mean I don't believe it is healthy to try
and make something look \"good\". Transparent windows, animations, and
other effects just put strain on the CPU. Worry about high-contrast
themes that are easy on the eyes, and about what font you should use
(League Mono)."
      ,(a "https://gnu.org/software/emacs"
	  "Emacs")
      " is a gorgeous text editor for GNU! It is favoured by computer
programmers (and many other people) because it has many nice features!
It was first written in the 70's, and as you might imagine, it "
      (i "looks") " like software from the 70's. This is what you see
when you first open Emacs:")
   ,(img "/css/emacs/badmacs.png"
	 "Emacs out of the box looks ugly")
   (p "The fact that it looks so cluttered is the reason many people shy
away from it, despite the fact that it's actually, objectively the "
      (i "best") " code editor out there! (Okay that may be a bit bias, but
many people would agree with me!)")
   (p "Now, Emacs is a tool. It's not made to look pretty, and many of
us don't mind it, but for the sake of marketing, let's make Emacs look
more modern & minimalistic. Take my Emacs startup window for
example:")
   ,(img "/css/emacs/goodmacs.png"
	 "Emacs after some configuration looks very nice.")
   (p "Believe it or not, I'm only using two external extensions for the
GUI (the dark theme and the green modeline)")
   (p "In this short article I'm going to go through every step in
configuring Emacs to make it look just like mine (but you can edit
whatever you want)")

;;; FROM THE OPTIONS MENU 
   (hr)
   (h2
    "From the Options Menu")
   (p "At the top of your window, next to \"File\" and \"Edit\", there
should be an \"Options\" button. From there we start by removing the
fluff we don't need and adding the fluff we want!:")
   (ul
    (li "Highlight Active Region -> Yes")
    (li "Highlight Matching Parentheses -> Yes")
    (li "Save Options"))
   (p "Alright, now still in the options menu, go to \"Show/Hide\", and
from there:")
   (ul
    (li "Toolbar -> None")
    (li "Tooltips -> No")
    (li "Scrollbar -> No")
    (li "Fringe -> Default")
    (li "Window Divider -> None"))

   (p "That's it for our Options menu configuration. Oh, and if you're
wondering, I use League Mono, font size 16.")
   (p "Now it's time for our .emacs file! The .emacs file is generally
located in your home directory: ~/.emacs")
   (p "If you cannot find a .emacs file, create it now.")

;;; MELPA
   (hr)
   (h2 "MELPA")
   (p "The "
      ,(a "https://melpa.org/"
	  "MELPA")
      " is an Emacs package archive. Almost everyone uploads their Emacs
packages to the MELPA, so we want to be able to access it. At the very
top of your .emacs file, put down:")

   (pre "(require 'package)
(add-to-list 'package-archives
	     '(\"melpa-stable\" . \"https://stable.melpa.org/packages/\"))")
   (p "Just below it you should see a line reading")

   (pre "(package-initialize)")
   (p "If you don't see it, add that in.")
   (p "Everything from now on will take place between")
   (pre "(package-initialize)")
   (p "And")
   (pre "(custom-set-variables)")

;;; POWERLINE
   (hr)
   (h2 "Powerline")
   (p "The Mode-line in Emacs is a little boring. Maybe you'd rather have
something a bit more show-y, like... pointy arrows down there!
Introducing "
      ,(a "https://github.com/milkypostman/powerline"
	  "Powerline") "!")
   (p "Before:")
   ,(img "/css/emacs/modeline.png" "Emacs has a modeline...")
   (p "After:")
   ,(img "/css/emacs/powerline.png" "But Powerline looks very nice!")

   (p "Download Powerline from the MELPA archive with:")
   (pre "M-x package-install RET powerline RET")
   (p "And, just underneath our previous .emacs code, write:")
   (pre "(require 'powerline)")

;;; MOE THEME
   (hr)
   (h2 "Moe Theme")
   (p ,(a "https://github.com/kuanyui/moe-theme.el"
	  "Moe Theme")
      " is a theme for Emacs that includes colour for text highlighting and
Powerline. It includes a dark theme and a light theme. You can install
Moe officially from the MELPA by running:")
   (pre "M-x package-install RET moe-theme RET")
   (p "After Moe is installed, go back to your .emacs file, and right
under the Powerline code we just typed in, add:")

   (pre "(require 'moe-theme)
(moe-theme-set-color 'magenta)
;;Available Colors: blue, orange, green, magenta, yellow, purple, red, cyan, w/b
(moe-dark)")

   (p "(moe-theme-set-color) sets the color for the Powerline. The
\"Available colours\" comment under it lists all the colours that you
can use instead of magenta.")
   (p "(moe-dark) is the dark Moe theme. (moe-light) can be used instead,
or, if you like light in the day & dark at night, use (require
'moe-theme-switcher) instead!")
   (p "One more thing, at the very bottom of your .emacs file (the very,
very bottom), type:")
   (pre "(powerline-moe-theme)")
   (p "This should make your Powerline look defined & stylish with the
Moe theme!")

;;; TRANSPARENT / TRANSLUCENT / FADED WINDOW / BUFFER
   (hr)
   (h2 "Transparent Window")
   (p "I use a slightly transparent window so I can see my nice wallpaper
through Emacs! Under the moe-theme code typed above, add in:")
   (pre "(set-frame-parameter (selected-frame) 'alpha '(92 . 90))
 (add-to-list 'default-frame-alist '(alpha . (92 . 90)))")
   (p "Change the numbers to higher or lower values depending on the
transparency you want!")

;;; STARTUP / SETUP / INIT TWEAKS / TIPS
   (hr)
   (h2 "Startup Tweaks")
   (p "Under our previous code, let's do a few things!")
   (p "Change the window title:")
   (pre "(set-frame-name \"Tulips Smell Nice\")")
   (p "Or if you'd rather have the entire file path in the title
bar:")
   (pre "(setq frame-title-format '(\"%f\" (dired-directory dired-directory \"%b\")))")
   (p "Change the startup scratch message:")
   (pre "(setq initial-scratch-message \";;Hello, Lily!\")")
   (p "Disable the beeping sound when hitting the end of a file:")
   (pre "setq ring-bell-function 'ignore")
   (p "Change the Minibuffer startup message:")

   (pre "(defun display-startup-echo-area-message ()
  (message \"Bake me a cake!\"))")

   (p "Alrighty! Pretty good so far, Just one more thing. When we save a
file, it gives us a \"backup\" file just next to it! This is handy
sometimes, but lets plop those in another directory, just to keep our
workplace clean:")

   (pre "  (setq backup-directory-alist
	  `((".*" . ,temporary-file-directory)))
    (setq auto-save-file-name-transforms
	  `((".*" ,temporary-file-directory t)))")
   (p "Perfect! If there's any really cool thing that you think is a
\"must have\" for Emacs users, let me know via Email (shack AT muto
DOT ca) or on XMPP: mutoshack AT 616 DOT pub")

;;; MY .EMACS .INIT FILE
   (hr)
   (h2 "My .emacs File")
   (p "Here's how the entire file should look when we're done:")

   (pre ";;Very top of file
;;MELPA
(require 'package)
(add-to-list 'package-archives
	     '(\"melpa-stable\" . \"https://stable.melpa.org/packages/\"))

(package-initialize)

;;Powerline
(require 'powerline)

;;Moe Theme
(require 'moe-theme)
(moe-theme-set-color 'magenta)
;;Colors: blue, orange, green, magenta, yellow, purple, red, cyan, w/b.
(require 'moe-theme-switcher)

;;Transparency
(set-frame-parameter (selected-frame) 'alpha '(92 . 90))
(add-to-list 'default-frame-alist '(alpha . (92 . 90)))

;;Titlebar
(set-frame-name \"Tulips Smell Nice\")

;;Scratch message
(setq initial-scratch-message \";;Welcome to Emacs!\")

;;Minibuffer message
(defun display-startup-echo-area-message ()
  (message \"Bake me a cake!\"))


;;Backup directory
  (setq backup-directory-alist
	  `((\".*\" . ,temporary-file-directory)))
    (setq auto-save-file-name-transforms
	  `((\".*\" ,temporary-file-directory t)))

(custom-set-variables
 ;;...
 ;;...
 ;;...


 ;;Down at the bottom
(powerline-moe-theme)")
   (p "Well, I have to go make myself a tea. Stay stylish!  -Muto")))
