(page
 :title "Opus Obsession"
 :date "Nov 29, 2019"
 :body
 `((cc "Dance in the morning sun
Go outside and breathe in the dew.
Your work is never done
There's nothing in this world for you")
(p "I like everything in my life to be simple and uniform, and I
spent some time creating scripts that ease the process of compulsively
reformatting every file I download.")
   (p "If you plan on modifying your files in any way, please make a
backup first. For instance, all my high resolution audio files are
copied to my second hard drive and compressed, just in case I need
them in the future!")
   (p "All my audio files are Opus because Opus sounds great at lower
bitrates compared to other codecs. I usually convert a downloaded MP3
to a 96kbps Opus file with FFMPEG by running the command:")
   (pre "ffmpeg -i song.mp3 -ab 96k song.opus")
   (p "Of course, music CD's usually have a lot of files that I need
to convert, so I wrote a script to do this for me:")
   (pre "#opusify.sh - convert all MP3's to Opus.

for i in *.mp3; do
   ffmpeg -i \"$i\" -ab 96k \"${i[@]/%mp3/opus}\"
   rm *.mp3
done")
   (p "I usually also edit the metadata of my music with "
      ,(a "https://picard.musicbrainz.org/"
	  "Picard")
      ".")
   (p "Now, sometimes when I buy a new CD and copy the songs to my
computer, an \"ls\" command looks like this:")
   (pre "That Really (Really) Long Band Name - 01 - A Song Title.mp3
That Really (Really) Long Band Name - 02 - The Second Song.mp3
That Really (Really) Long Band Name - 03 - The Remix You Didn't Need.mp3
That Really (Really) Long Band Name - 04 - The Song You Always Skip.mp3
That Really (Really) Long Band Name - 05 - The One All Your Friends Talk About.mp3")
   (p "On a portable device, the entire filename might not even be
displayed, and I don't like that, so I made another script that can
replace or remove strings from filenames:")
   (pre "#filenamer.sh - Replace strings from filenames

for i in *.opus; do
   mv \"$i\" \"${i/old/new}\"
done")
   (p "In the above case, instead of:")
   (pre "i/old/new")
   (p "We would write:")
   (pre "i/That\\ Really\\ \\(Really\\)\\ Long\\ Band\\ Name\\ -\\ /")
   (p
    "\"\\\" is an "
    ,(a "https://en.wikipedia.org/wiki/Escape_character"
	"escape character")
    ", which we need for spaces and some special characters (like
parenthesis).")
   (p "And now everything looks right!")
   (pre "01 - A Song Title.mp3
02 - The Second Song.mp3
03 - The Remix You Didn't Need.mp3
04 - The Song You Always Skip.mp3
05 - The One All Your Friends Talk About.mp3")
   (p "I like that! Let's move on to my last scenario.")
   (p "Let's say you buy a CD and copy the music to your computer.
The files look like this:")
   (pre "Track1.opus
Track2.opus
Track3.opus
Track4.opus
Track5.opus")
   (p "Well darn. I can't tell which song comes first without looking
at the back of the CD case!")
   (p "Thankfully (ideally), most songs come with metadata that tell
us the track number, song title, artist name etc.. We just need a
script that can take the metadata from the song, and rename the file
corresponding to that metadata! For this I used an existing program
called "
      ,(a "https://github.com/hcmiya/opuscomment"
	  "opuscomment")
      ", which, thankfully, was very easy to compile.")
   (p "The script for converting every song name to its metadata name
looks like this:")
   (pre "#!/bin/bash

#autoTAG.sh - Rename the song file to the track number followed by the track title.

for i in *.opus; do
   opuscomment '$i' |
   egrep -i 'tracknumber|title' |
   sed 's/tracknumber=//I; s/title=//I' |
   sed -z 's/\\n/ /' > file.txt
   mv '$i' '$(cat file.txt).opus'
done

rm file.txt")
   (p "Admittedly, this is not beautiful, but now the files all look
like this:")
   (pre "1 Some Good Song.opus
2 Another Great Tune.opus
3 If I Wrote a Song.opus
4 My Music Would Be Banned.opus
5 From America.opus")
   (p "Finally, my music collection is the way I want it, perfect for
playing in "
      ,(a "https://mpv.io"
	  "MPV")
 ".")))
