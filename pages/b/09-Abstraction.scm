(page
 :title "Abstraction Abuse"
 :date "Nov 24, 2019"
 :body
 `((cc "The clack of a keyboard,
The blast of a gun,
Everything was picked up
While the audio tape spun.")
   
   (p "An abstraction is the simplification of... Things. We use
abstractions to make our computer usage easier (way easier). We might
use libjpg and libpng to load images, or OpenGL and Vulkan to work on
a 3D plane. Abusing abstractions is when someone uses a high-level
abstraction to perform a single low-level function, or making
something so abstract that it simply crashes your computer.")
   (p "Lower-level abstractions can be extremely helpful, and are, in
fact, healthy, since they can carry out complex tasks efficiently. "
(i "\"Abstraction abuse\"") " means programs are created at such a
high-level that they become" (i "large and inefficient") ". Other
titles I considered for this post include " (i "\"Arbitrary
Abstraction\"") ", " (i "\"Toxic Abstraction\"") ", and " (i "\"When
Abstraction Becomes a Design Fallacy\"") ".")

   (hr)
   (h2 "Over-Abstracting")
   (p "Over abstracting, or making a tool to be used at a very high
level, can create an awkward and narrow workflow and result in
inefficient \"low-level mimicking\". Here's a non-computer related
example: A square is a rectangle with even sides. A rectangle is made
up of 4 connecting lines. A line is made of two points on a 2D plane,
and so on.")
   (p "If your high-level documentation doesn't tell users what a line
is (or possibly, your entire program doesn't " (i "allow") " users to
make a line), and they need a line, they'll likely just make a long,
narrow rectangle, or stack a bunch of small squares next to one
another! If you think about this \"line\" as something read by a
computer, you can bet that it will take longer to draw.")
   (p
    ,(a "https://github.com/electron"
     "Electron by GitHub")
    "is probably the " (i "best") " example of over-abstraction.
Electron allows people to create desktop applications (let me use
quotes for that: " (i "\"Desktop\"") " applications) with Web
technologies (JS, CSS, HTML). This means every Electron application
has to be shipped with JavaScript, CSS, and HTML " (i "rendering
engines") ". You are pretty much " (i "opening a web browser") " to a
local site whenever you launch an Electron program (and, since web
rendering engines are so big, Electron programs also take up a lot of
space).")
   (p "As you may have guessed, Electron is not for efficient
programmers. It's for programmers who want to ship out a product
across " (i "multiple platforms") " fast. I won't say using Electron
is not a good business tactic, but I can say that it will, almost
definitely, provide users with a " (i "sub-par") " experience.")
   (p "Can Electron programs output to stdio? I have never tried it. I
know, I should try a tool before judging it.")

   
   (hr)
   (h2 "The Implementation Issue")
   (p "One side of Abstraction abuse is simply the " (i "refusal") "
of using someone else's abstractions at all, and opting to write your
own, because " (i "you can only truly trust yourself!")  " I can
understand this from a hobbyist point of view, since it's always fun
to implement your own version of the fundamental algorithms, but these
\"home-grown\" algorithms are often both bigger and less efficient
than standard, common libraries.")
   (p "I kind of want to mention "
    ,(a "https://en.wikipedia.org/wiki/Greenspun%27s_tenth_rule"
        "Greenspun's 10 Rule of Programming") " here:")
   (p (i "Any sufficiently complicated C or Fortran program contains
an ad-hoc, informally-specified, bug-ridden, slow implementation of
half of Common Lisp."))
   (p "Although this is just rhetorical Common Lisp propaganda, it
holds the same idea of poor algorithm implementation.")
   (p "(And no, there are no 9 other rules. I don't know why,
either.)")

   
   (hr)
   (h2 "Disk usage")
   (p "Poorly implemented or obsessive abstraction can lead to " (i
"heavier programs") ". Dynamic-linking has partially solved this
problem, but it's still a huge pain when it comes to things like " (b
"compiling") " and " (b "sandboxed package management") ". One main
problem I come across with dynamically-linked programs is when the
program uses a strange, non-standard library that isn't in any
mainstream package manager (and if it is, it's a hundred years newer
than the specified version & is no longer backwards-compatible at
all). Another problem is when a program uses a library that is simply
just a set of abstractions for, say GTK, despite GTK being a decently
high-level library. The programmer may not use every function in this
hypothetical GTK abstraction library, which can lead to inflating your
disk space usage (of course, it depends on how big the library really
is)")
   (p "A good example of this section is when someone wants to write a
program that does nothing but output a green square on a window, so
they use a video game engine and draw a square, then ship the entire
engine with their program. An optimized version is a program that just
uses libGL and GLUT.")
   
   (hr)
   (h2 "More resources")
   (p "Since I'm probably the least credible source ever, here are
some articles that hold a bit more integrity:")
   (ul
    (li ,(a "https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/"
           "Things you should never do"))
    (li ,(a "http://udidahan.com/2009/06/07/the-fallacy-of-reuse/"
      "The        Fallacy of Reuse")))
   (p
    "Are there any more articles related to computer abstraction?    I'd like to make this list bigger")))
