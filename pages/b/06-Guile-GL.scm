(page
 :title "Painting in Guile With OpenGL"
 :date "Dec 05, 2018"
 :body
 `((cc "As waves crashed in the sea,
The skull inside my head broke free.")
   (p ,(a "https://opengl.org"
	  "OpenGL")
      " is a common tool used for drawing shapes (like
squares, circles, cubes, etc.) on your computer screen. You are able
to rotate and scale these shapes, as well as illuminate them with
lights (and many other interesting graphical tricks)!")
   (p ,(a "https://gnu.org/s/guile-opengl"
	  "Guile-OpenGL")
      " is a set of bindings that allow us to access OpenGL's
functions from the "
      ,(a "https://gnu.org/s/guile"
	  "Guile")
      "programming language! This article demonstrates
Guile-OpenGL's usage and syntax through short explanations and
examples. I do not assume any prior experience of OpenGL or Guile.")

;;; INSTALLATION   
   (hr)
   (h2 "Installation")
   (p "If you are using the "
    ,(a "https://gnu.org/s/guix"
        "Guix")
    " package manager, you can install both Guile and Guile-OpenGL
with:")
   (pre "guix package -i guile guile-opengl")
   (p "If you are using some other package manager (like Apt, Aur,
Yum, or RPM), you can try searching them for Guile and
Guile-Opengl. You can also download the source package for "
    ,(a "https://gnu.org/software/guile/download"
        "Guile")
    " and "
    ,(a "https://ftp.gnu.org/gnu/guile-opengl/"
     "Guile-OpenGL")
    ".")
   (p "Once you have Guile-OpenGL installed, let's test it out with a
short program that simply displays a black window with no shapes in
it, so create a new file named \"blank.scm\" and put the following
code in it:")
   (pre "(use-modules (gl) (glut))
(make-window "Gaze into the abyss")
(set-display-callback (lambda ()
                        (gl-clear (clear-buffer-mask color-buffer))
                        (swap-buffers)))")
   (p "You can run the program with")
   (pre "guile blank.scm")
   (p "or within a Guile REPL:")
   (pre "(load \"blank.scm\")")
   (p "(disclaimer: if you get an error about \"swrast\" drivers
malfunctioning, it may be because you are using an NVIDIA driver. The
program runs fine under Nouveau but I don't know how to make it work
under NVIDIA drivers. Sorry.)")
   ,(img "/css/opengl/blank.png"
	 "A solid black window")
   (p "This very short program does a few things. We start off by
telling Guile which modules to use - "
    (i "(gl) and (glut)")
    " -    which contain the main OpenGL functions and the "
    (i "OpenGL Utility Toolkit")
    " respectively.")
   (p "We make a window with "
    (b "(make-window)")
    ". We also give our window a name - \"Gaze into the abyss\" (which
is part of a quote by Friedrich Nietzsche, when he talks about playing
"
    (i "Monster Hunter")
    ").")
   (p "We then set a display callback with "
    (b "(set-display-callback)")
    ", which, to my understanding, says \"Determine which window we're
going to draw on, then draw on it\". We pass two parameters to this
function - "
    (b "(gl-clear (clear-buffer-mask color-buffer))")
    ", which clears the color buffer (our background). The second
parameter is "
    (b "(swap-buffers)")
    ", which swaps the front and back buffers of a double buffered
window")
   (p "If you comment out "
    (b "(gl-clear (clear-buffer-mask color-buffer))")
    " and/or "
    (b "(swap-buffers)")
    ", you'll notice that the window becomes clear. We don't want a
clear window, nobody likes clear wind- Oh wait... Nobody likes clear
windows besides *literal* windows!")
   
   (hr)
   (h2 "Drawing a square")
   (p "Now that we're done drawing nothing, let's see if we can draw
something! A green 2D square is simple enough. Last I checked, a
square is a four-cornered polygon. Let's open up a new file
(square.scm) and write the following program:")
   (pre "(use-modules (gl) (glut))

(define (draw)
  (gl-clear (clear-buffer-mask color-buffer))
  (gl-begin (begin-mode polygon)
            (gl-color 0 1 0)
            (gl-vertex -0.3  0.3)
            (gl-vertex  0.3  0.3)
            (gl-vertex  0.3 -0.3)
            (gl-vertex -0.3 -0.3))
  (swap-buffers))

(make-window "A green square!")
(set-display-callback (lambda () (draw)))
(glut-main-loop)")
   ,(img "/css/opengl/greensquare.png"
	 "A green square")
   (p "As you can see, we defined our square as a function "
    (b "(draw)")
    " and call it inside set-display-callback. This makes our program
look very clean (this practice is helpful when you're drawing many
things at once).")
   (p "Since a square is a polygon, we use "
    (b "(gl-begin (begin-mode polygon))")
    " to tell OpenGL that we're going to draw a polygon. We then tell
OpenGL what color we want our square to be (green, since the three
numbers after gl-color are Red, Green, and Blue. 1 1 1 is white, 0 0 0
is black), then we point out where our vertices (corners) are going to
be. We specify each corner one-by-one in a clockwise (or
counter-clockwise) manner. If we place our corners in a sort of X
shape, for example:")
   (pre "(gl-vertex -0.3 0.3)
(gl-vertex 0.3 -0.3)
(gl-vertex 0.3 0.3)
(gl-vertex -0.3 -0.3)")
   (p "then we would end up with a funky looking square.")
   ,(img "/css/opengl/funkysquare.png"
	 "A green square, with a triangular chunk missing")
   (p "If we add a "
    (b "(gl-color)")
    " line before each vertex, then each corner of our square will be
a different color!")
   (pre "(gl-color 0 1 0)
(gl-vertex -0.3 0.3)
(gl-color 1 0 0)
(gl-vertex 0.3 0.3)
(gl-color 0 0 1)
(gl-vertex 0.3 -0.3)
(gl-color 1 0 1)
(gl-vertex -0.3 -0.3)")
   ,(img "/css/opengl/colorsquare.png"
	 "A square, every corner is a different colour")
   (hr)
   (h2 "Drawing 3D models")
   (p "3D modeling is what OpenGL was built for. The OpenGL Utility
Toolkit (GLUT) comes with a few 3D models that we can use right off
the bat (glutSolidSphere, glutSolidTeapot, glutSolidCube, and more)!
Lets try drawing a teapot with "
    (b "glutSolidTeapot")
    ". For this we'll need the "
    (b "(glut low-level)")
    " module, but don't worry, it's not "
    (i "actually")
    " low level. It just gives us access to certain functions that
haven't been renamed in the Guile bindings yet (one of which is
glutSolidTeapot):")
   (pre "(use-modules (gl) (glut) (glut low-level))

(define (draw)
  (gl-clear (clear-buffer-mask color-buffer))
  (gl-color 1.0 0.1 0.0)
  (glutSolidTeapot 0.5)
  (swap-buffers))

  (initialize-glut
                   #:window-size '(800 . 800))
  (make-window \"A lovely red teapot\")
  (set-display-callback (lambda () (init)))
  (glut-main-loop)")
   ,(img "/css/opengl/noshadepot.png"
	 "A flat red teapot. No shading.")
   (p "Wow cool... Wait hold up! This teapot doesn't look 3D at all!
We've been smeckledorfed! Well actually, as you might have guessed, we
need a light source to illuminate our subject. To do this we need the
"
    (b "(gl low-level)")
    " module, which contains the glLight function, and we need to use
"
    (b "(gl-enable)")
    ", which lets us enable certain functionality (such as
lighting).")
   (pre "(use-modules (gl) (gl low-level) (glut) (glut low-level))

(define (init)
  (gl-enable (enable-cap light0))
  (gl-enable (enable-cap lighting))
  (gl-enable (enable-cap color-material))
  (glLightf (light-name light0) (light-parameter position) 1.0))

(define (draw)
  (gl-clear (clear-buffer-mask color-buffer)
  (gl-color 1.0 0.1 0.0)
  (glutSolidTeapot 0.5)))

(define (on-display)
  (init)
  (draw)
  (swap-buffers))

(initialize-glut
                 #:window-size '(800 . 800))
(make-window \"Guile OpenGL\")
(set-display-callback (lambda () (on-display)))
(glut-main-loop)")
   ,(img "/css/opengl/shadepot.png"
	 "A shaded red teapot.")

   (p "As you can see by this example, we define " (i "two") "
functions, " (b "(init)") " and " (b "(draw)") ".  (init) holds all
the (gl-enable) procedures along with the " (b "glLightf") " function,
which (draw) does the actual drawing. " (b "(on-display)") " holds all
our functions together, which can all be called together in our
display callback function.")
   (p "We enable " (b "light0") ", which is the name of the light
source we're going to use (light0, light1, light2, etc.  are different
light sources we can enable). We also enable " (b "lighting") ", which
allows light0 to shine.  " (b "(color-material)") " lets us use the
red color of our teapot. If you comment out this line, the teapot will
be grey.")
   (p (b "glLightf") " (the f at the end stands for \"float\") lets us
change the parameters to our light. We specify the light that we want
to change (light0), and tell OpenGL which setting should be changed -
in this case, we want to change the position of the light.")
   (p "(At the time of writing, I am only able to illuminate the
subject from the left or right with 1.0 and -1.0. If anybody knows how
to use this function correctly, with 3 floats in a list or array,
please contact me: shack[at]muto[dot]ca )")
   
   (hr)
   (h2 "Setting fog, distance, and background color")
   (p "Let's wrap this article up with a somewhat complex program.
We're going to draw 5 red teapots all at different distances from the
camera, and make the ones further away fade out into fog. I'll make a
new file (fog.scm) and write the following program in it:")
   (pre "(use-modules (gl) (gl low-level) (glut) (glut low-level) (glu))

(define (init)
  (set-gl-clear-color 0.0 0.02 0.02 1.0)
  (gl-enable (enable-cap light0))
  (gl-enable (enable-cap lighting))
  (gl-enable (enable-cap color-material))
  (gl-enable (enable-cap depth-test))
  (gl-enable (enable-cap fog))
  (glPushMatrix)
  (set-gl-matrix-mode (matrix-mode projection))
  (gl-load-identity)
  (glu-perspective 90 1 0.1 20)
  (set-gl-matrix-mode (matrix-mode modelview))
  (glPopMatrix))

(define (my-lighting)
  (glLightf (light-name light0) (light-parameter position) 1.0)
  (glFogi (fog-parameter fog-mode) (fog-mode exp))
  (glFogf (fog-parameter fog-color) 0.0)
  (glFogf (fog-parameter fog-density) 1.0)
  (glHint (hint-target fog-hint) (hint-mode nicest)))

(define (red-teapot x y z)
  (glPushMatrix)
  (gl-translate x y z)
  (gl-color 1.0 0.1 0.0)
  (glutSolidTeapot 0.2)
  (glPopMatrix))

(define (draw)
  (gl-clear (clear-buffer-mask color-buffer depth-buffer))
  (red-teapot -0.2 -0.2 -0.5)
  (red-teapot  0.0  0.0 -1.0)
  (red-teapot  0.4  0.4 -2.0)
  (red-teapot  1.0  1.0 -2.9))

(define (on-display)
  (init)
  (my-lighting)
  (draw)
  (swap-buffers))

(initialize-glut
 #:window-size '(800 . 800))
(make-window "Some foggy teapots")
(set-display-callback (lambda () (on-display)))
(glut-main-loop)")
   ,(img "/css/opengl/fog.png"
	 "Several teapots at different distances, fading away.")
   (p "Okay, so I'll set the new functions in a list:")
   (ul
    (li (b "(set-gl-clear-color)")
     " sets the background color.")
    (li (b "(depth-test)")
     " makes sure that all the teapots render correctly.")
    (li (b "(glPushMatrix)")
     " keeps the current matrix so that nothing gets done more than
once.")
    (li (b "(set-gl-matrix-mode (matrix-mode projection))")
     " allows us to set a projection matrix, which is important for
our point-of-view")
    (li (b "(gl-load-identity)")
     " replaces the current matrix with the \"identity\" matrix.")
    (li (b "(set-gl-matrix-mode (matrix-mode modelview))")
     " switches the matrix mode back to modelview, which is used for
drawing our teapots.")

    (li (b "(glFogi)") " lets us change the fog settings. We use " (b
"exp") " mode and set the " (b "color") " to black. We set the fog "
(b "density") " to 1.0 and the fog " (b "hinting") " to nicest.")
    
    (li "We define our red teapot as a function (red-teapot) with
three arguments (x y z), which allows us to use the function
red-teapot as many times as we want, which we do 5 times in (draw)."))
   (p "(the fog color is the same issue as with the light position.  I
can only switch between red and black fog)")
   
   (hr)
   (h2 "Conclusion")
   (p "This article, although not a replacement for the Redbook (link
below), hopefully helps someone out there (maybe it was
you!). Guile-OpenGL, although it's not maintained much anymore, is a
very efficient and promising set of bindings for OpenGL development in
Guile! Procedurally generated images and graphical recursive functions
feel very natural in Guile-OpenGL and is a powerhouse for algorithmic
creativity!")
   (p "As for learning resources, I'd recommend "
    ,(a "https://www.gnu.org/software/guile-opengl/manual/"
        "the Guile-OpenGL documentation")
    ", "
    ,(a "https://www.khronos.org/opengl/wiki/Common_Mistakes"
        "Common OpenGL mistakes")
    ", and of course, the "
    ,(a "http://opengl-redbook.com/"
        "Redbook"))
   (p
    "Also, check out the bi-annual "
    ,(a "https://itch.io/jam/lisp-game-jam-2018"
        "Lisp Gamejam"))
   (p "That's it for this article. Stay sharp! -Muto")))
