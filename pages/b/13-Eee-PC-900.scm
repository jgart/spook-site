(page
 :title "The Eee PC 900 is Good"
 :date "Feb 08, 2020"
 :style
 "table {background: black; margin: .5em;}
  th {background: #AAAAAA; border: 1px solid black; padding: .1em .3em;}
  tr:nth-child(odd)  {background: #EEEEEE; border: 2px solid;}
  tr:nth-child(even) {background: #CCCCCC; border: 2px solid;}
  td {border: 1px solid; padding: .1em .3em}"
 :body

 `((cc "The sun was cold and the sky was pale
And alfred the dog was out chasing his own tale
And the rooftops were on fire
and the roads were underwater
And the anonymous byer
Bought drugs from the mayer's daughter.")
   
   (p "The ASUS Eee PC 900 is a 2008 9-inch display netbook.")
   ,(img "/css/eeepc/1plant.jpg"
	 "[Photo of a small laptop]")
   (p "I found the tiny machine at a thrift store a few years ago, and
have been using it ever since. I upgraded the 20GB hard drive to a
120GB SSD, and upgraded the bios to a version that supports
mSATA. It's really hard to find a small laptop anymore (Sorry, big
companies, 12-inches isn't \"ultra portable\")!")
   (p "Whoever the original owner was had a sense of style, too. So
thanks to whoever put this sticker on the front, because it makes this
thing look pretty rad:")
   ,(img "/css/eeepc/1sticker.jpg"
	 "[A glittery sticker of a bird and a flower]")

;;; THE KEYBOARD
   (hr)
   (h2 "The Keyboard")
   (p "The keyboard is small. It's about as good as you can make a
chiclet keyboard of this size. I'll admit it's not the most
comfortable thing in the world, but I was surprised that it actually
doesn't feel as bad as it looks.")
   ,(img "/css/eeepc/4keyboard.jpg"
	 "[The cramped keyboard]")
   (p "The Control key is awkward to press, which is a pain sometimes,
but it's not the worst. All in all, it works when I'm out and about,
and it makes me just a bit more grateful to have my full-sized
keyboard to come home to!")

;;; HARDWARE
   (hr)
   (h2 "Hardware")
   (p "This machine packs quite a few ports, considering the size.
It's almost like ASUS was trying to make the best system administrator
machine in the world. On one side you have Ethernet, a USB 2 port, a
microphone hole and a headphone hole.")
   (p "On the other side there's a VGA port, two more USB 2 ports, and
an SD card reader!")
   ,(img "/css/eeepc/3ports.jpg"
	 "[Photos of all the ports]")

;;; MODULARITY
   (hr)
   (h2 "Modularity")
   (p "There is a surprisingly large market for old Eee PC upgrades. "
    ,(a "https://www.amazon.ca/Superb-Choice%C2%AE-6-Cell-Battery-Compatible/dp/B07JQFR6SS/ref=sr_1_3?keywords=asus+eeepc+900+battercy&qid=1581029968&sr=8-3-spell"
     "    batteries")
    " and "
    ,(a "https://www.amazon.ca/JMT-mSATA-Adapter-Converter-3x5cm/dp/B0817PW5DN/ref=sr_1_1?keywords=msata+eeepc+900&qid=1581030351&sr=8-1"
     "mSATA SSD Converters")
    " can be purchased for a relatively good price (~$50 for a 6600
Mah battery).")
   (p "Speaking of upgrades, the 900 has a little door on the back
that can be opened up to see both the hard disk and the RAM module,
which makes it great for quick and easy maintenance!")
   ,(img "/css/eeepc/5rear.jpg"
	 "[The bottom of the laptop]")
   ,(img "/css/eeepc/6open.jpg"
	 "[The bottom door opens up]")

;;; PORTABILITY   
   (hr)
   (h2 "The Portability")
   (p "I mean, this thing's small. It's probably the smallest laptop
I've ever used.")
   ,(img "/css/eeepc/smawl2.png"
         "[Smallest things in the world: Atom, Quark, Eee PC]")
   (p "I prefer buttons to touchscreens, and I don't like the gloss of
tablet screens, so this is pretty awesome to have as my out-and-about
machine! The best thing is that this is the only PC small enough to
fit in my over-shoulder bag with room to spare:")
   ,(img "/css/eeepc/8bag.jpg"
         "[A green over-shoulder bag containing the laptop]")
   (p "Walking downtown with this thing feels cool, even if I don't
use it. It's like when you're a kid and you wear cool socks and you
feel like the socks make you cooler. This laptop is my cool socks.")
   ,(img "/css/eeepc/7music.jpg"
         "[CMUS Music Player playing on the Eee PC 900]")

;;; COMPARISON   
   (hr)
   (h2 "A Comparison")
   (p "Some people think that if there's a newer computer than theirs,
they need to upgrade. Back when Moore's Law was relevant, maybe it
would make sense, but times have changed.  Major breakthroughs in
computer science are getting less frequent. Computers considered
\"old\" (pre-2010) can still run modern UNIX at lightspeed! Maybe it's
a bit tougher to, say, create complex 3D models or edit HD films on an
older machine, but you can definitely hack around on one just fine,
making them great for me.")
   (p "To visualize this claim, I want to compare this model with
Kate's laptop from the 1995 hit film \"Hackers\", just because it's a
good movie.")
   (table
    (tr
     (th "[ Component ]")
     (th "[ Powerbook Duo 270c ]")
     (th "[ ASUS Eee PC 900 ]"))
    (tr
     (td "CPU")
     (td "33MHz 68030")
     (td "900MHz Celeron M"))
    (tr
     (td "OS")
     (td "Unix-unlike")
     (td "Unix-like"))
    (tr
     (td "RAM")
     (td "32MB")
     (td "2GB"))
    (tr
     (td "Storage")
     (td "240MB HDD")
     (td "20GB SSD"))
    (tr
     (td "Modem")
     (td "28.8 Kbps")
     (td "No modem")))
   (p "These laptops are from two totally different eras, but if that
1993 PowerBook could run Apple's advanced UNIX-like system 7 in a
million psychedelic colours, then I wouldn't call a computer that's
~27x more powerful \"outdated\". If I were a teenager in a 90's movie,
I'd love to have this thing.")
   (p "(And yeah, it looks crispy in the dark).")))
