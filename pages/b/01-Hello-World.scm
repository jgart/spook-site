(page
 :title "Hello, World!"
 :date  "Jan 19, 2018"
 :body
 `((cc "So this is everything.")
  (p "So this is the first blog post on my website (muto.ca). I want
      to share my thoughts about coding as a walk of life
      rather than as a job. I am a software developer
      professionally, but I don't believe the world of
      computers is limited to people like me, I think it's a
      very enjoyable activity, whether you like math or not
      (I'm not big on math, myself...) Anyway, I'd better be
      off. I still got things to do!")))
