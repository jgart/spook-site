(page
 :title "Electron is Actually Really Good"
 :date "Apr 01, 2020"
 :body
 `((cc "We're all alone.")
   (p "A long time ago I was pretty dumb, and I said some pretty
biased and extreme stuff against GitHub's "
    ,(a "https://electron.io"
	"Electron")
    " framework. This article is a rebuttle for stuff I said in the
past because I gave it some thought and changed my mind about most of
it.")
   (p "In short, Electron is actually a really awesome framework and I
shouldn't have been mean about it!")

;;; ELECTRON USES A LOT OF MEMORY   
   (hr)
   (h2 "\"Electron uses a lot of memory!\"")
   (p "No, absolutely not.")
   (p "Sure, maybe an elementary program using Electron uses nearly
100 Megabytes of memory on average, but let's be real, when's the last
time I had to use a device that had less than 100MB of memory? I don't
think they make memory modules that small anymore! All my machines
have 8 Gigabytes " (i "at least") ", which means I can run " (i
							      "lots") " of Electron apps at the same time! Computers are always
getting faster, and this allows us to use more resources!")
   (p "Allow me to visualize my point with an image:")
   ,(img "/css/Electron-good/amount.png"
         "A graph showing that Electron *only* uses half the RAM your
computer has, leaving room for maybe a whole other Electron
program!")
   (p "It's a ridiculous argument to say Electron uses too much memory
in this day and age. Back in the day, people joked that EMACS stood
for \"Eight Megabytes And Constantly Swapping\". Do you still believe
Emacs is a memory hog?")

;;; ELECTRON APPS AREN'T UNIFIED   
   (hr)
   (h2 "\"Electron Apps Aren't Unified with the OS!\"")
   (p "Seriously? " (i "this") " is what you call an argument?")
   (p "Oh, boo hoo for you! Electron apps aren't written in GTK, but
you know what? My Electron App's CSS UI is scalable, and colorful, and
it looks just how I want my program to look. I think it's way better
that my program has a sense of uniqueness among all your \"it looks
the same\" GTK or Qt programs!  Seriously, let us have a sense of
style now and then. It's not hurting anyone!")
   (p "Don't even talk to me with that \"I want to be as absolutely
boring as possible\" attitude! I mean, really.")

;;;ELECTRON APPS ARE WEBAPPS   
   (hr)
   (h2 "\"Electron Apps are Glorified Webapps!\"")
   (p "What the heck does this even mean? This doesn't make any
sense!")
   (p "Wrong again, young me. Electron Apps have access to your
Operating System's API's and system calls. Electron interfaces with
your native environment in a way webapps " (i "never") " could! You
can not only use Electron as a means for users to download your
webapp, but you can use the interface it provides to give users
invaluable complex abstractions!")
   (p "And to add to all this, there's nothing wrong with Webapps to
begin with! The Web is a beautiful, powerful thing, and being able to
have all the power the W3C gives us and take it to our offline
machines is a blessing!")

;;; ELECTRON IS FOR LAZY PROTOTYPING   
   (hr)
   (h2 "\"Electron is for Lazy Prototyping\"")1
   (p "Define \"lazy\" for me, because you probably fit the definition
for not educating yourself on the beauties of Electron!")
   (p "Electron is " (i "not") " for prototyping at all. Electron
should be used for the " (i "finished product") " that is going to be
delivered to the customer as soon as possible. Electron allows you to
ship your project to every major operating system available! No, BSD
is not a \"major operating system\". If you run something as obscure
as BSD, just download Windows already!  Windows is clean, and sane,
and supported!")
   (p "No, really, if you use " (i "anything") " besides Windows, you
should probably just download Windows. I have never had more than a
few problems a day on Windows, so I believe it is pretty much the best
thing you can have on a computer.")

;;; ELECTRON IS NOT REALLY FOR LINUX   
   (hr)
   (h2 "\"Electron is Not Actually for Linux\"")
   (p "Yes it is, actually!")
   (p "Oh, are you talking about the fact that Electron only works on
x86_64 hardware? Wow, you must be " (i "really") " old if you think
that's an argument! Nothing isn't x86_64 anymore! Some hardware like
Raspberry Pi or the EOMA68 or older x86 computers won't support your
Electron program, but let's be real, that's 5, maybe 6 people that
won't have access to your program.  " (i "Everyone") " else has moved
on to x86_64 hardware! When I was a kid and I outgrew my shoes, I
bought new shoes. I think the computing industry needs to outgrow
older architectures!")
   (p "Buy new shoes, and by that I mean buy a $1,000 computer,
because you need to every half-decade if you want to keep up with the
software you want to use. Definitely not just because computer
companies need to keep their profits high (which is " (i "also") " a
good reason to buy a new PC!).")

;;; CONCLUSION   
   (hr)
   (h2 "A Conclusion")
   (p "There's a lot of stigma around being an Electron developer in
this day and age. They will say you're not a \"real\" programmer just
because you work on a much higher level than they do, because they
write in bytecode (well, C, which is pretty much just bytecode) and
think it's the best thing ever.  It's like if people told you that you
weren't a good carpenter because you didn't go to the woods and chop
down your own trees, but you instead went to IKEA to buy a table. You
still need to " (i "build") " the table, so you should still be
considered just as much a carpenter as them!")
   (p "If you have any comments, be sure to send me a Gmail!")
   (p "I'm finally making the switch from the old, outdated "
      ,(a "https://gnu.org/software/emacs"
	  "eMacs")
      " program to    the new and cool "
      ,(a "https://atom.io"
	  "Atom")
    " editor (built on Electron)! I finally feel like I know how to
use computers " (i "properly") ". That's it for this article. Have a
great April 1st. ;-)")))
