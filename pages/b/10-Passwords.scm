(page
 :title "Clean Password Management"
 :date "Nov 24, 2019"
 :body
 `((cc "I'm never a winner on those popsicle sticks.")
   (p "I'm writing this post because of the current push towards
online password managers. You're putting all your passwords on someone
else's computer (a computer that anyone can access, no less). You not
only have no idea how secure your passwords are, but you probably
don't even know " (i "if") " your passwords are secured at all!
Anyway, on with the article.")
   (p "I'm going to let you in on a little secret: " (i "I have the
memory of a goldfish and can't be trusted with any sort of important
data whatsoever.")  " That's why I use a clean and sane way to store
my passwords: with "
    ,(a "https://passwordstore.org"
     "pass")
    " and "
    ,(a "https://gnupg.org"
     "GPG")
    ".")
   (hr)
   (h2 "Creating a PGP Key")
   (p "To encrypt my passwords, I have a PGP key. I created one by
installing GPG:")
   (pre "guix package -i gpg")
   (p "(The packages '" (b "gpg") "' or '" (b "gpg2") "' should be
available in most package managers)")
   (p "Then I generated my key with:")
   (pre "gpg quick-gen-key")

   (p "I was asked to name my key, I put in \"" (b "Ben's Key") "\"
and my key was generated!")
   (p "Read more about creating keys "
    ,(a "https://www.dewinter.com/gnupg_howto/english/GPGMiniHowto-3.html#ss3.1"
     "here")
    ".")

   
   (hr)
   (h2 "Creating the Password Store")
   (p "I installed the pass password manager:")
   (pre "guix package -i password-store")
   (p "(The packages '" (b "password-store") "' or '" (b "pass") "'
should be available in most package managers)")
   (p "Then I initialized my password store with:")
   (pre "pass init \"Ben's Key\"")
   (p "This creates a folder " (b "~/.password-store") " and encrypts
it with the GPG key \"" (b "Ben's Key") "\".")
   (p "Now I can create, remove, and organize my passwords
instantly:")
   (pre "pass insert favourites/example.com
pass generate email/example2.com")

   (p "Run '" (b "info pass") "' for a list of examples.")
   (hr)
   (h2 "Keeping my Passwords on a USB Drive")
   (p "For the novelty of it, let's put the encrypted " (b
".password-store") " folder onto a USB drive so that your GPG key and
passwords aren't always in one place.")
   (p "Create a directory to use as a mount point with:")
   (pre "mkdir ~/.pass-usb")
   (p "Plug the USB drive into your computer and mount it on our mount
point:")
   (pre "mount /dev/sdX ~/.pass-usb")
   (p "(Where the " (b "X") " in 'sd" (b "X") "' is the drive
identifier.)")
   (p "Move the " (b ".password-store") " directory onto the drive
(which is now mounted on " (b ".pass-usb") "):")

   (pre "mv ~/.password-store .pass-usb")
   (p "And finally tell pass where our new password directory is:")
   (pre " export PASSWORD_STORE_DIR=\"$HOME/.pass-usb/.password-store\"")
   (p "(You may want to put the above line at the bottom of your
~/.bashrc or ~/.profile)")
   (p "Now, if Ronald McDonald breaks into your house, steals your
computer *and* knows your GPG password, he won't be able to read your
passwords without the USB drive (but that may be the least of your
worries)")

   
   (hr)
   (h2 "Conclusion")
   (p "Initially I thought this would be a \"Well, duh!\" kind of
article, but after seeing countless ads for online password managers,
I figured a counterweight would be nice. Also, well, I just really
like pass.")
   (p "If you're interested in better password management, check out
these links:")
   (ul
    (li ,(a "https://csrc.nist.gov/csrc/media/publications/white-paper/1985/12/26/dod-rainbow-series/final/documents/std002.txt"
      "        DoD Password Management Guideline")
     ".  ")
    (li ,(a "https://www.troyhunt.com/only-secure-password-is-one-you-cant/"
            "The Only Secure Password is the One You Can't Remember"))
    (li     ,(a "https://xkcd.com/936/"
      "This XKCD comic"))
    (li ,(a "https://www.baekdal.com/thoughts/password-security-usability/"
      "        The Usability of Passwords"))
    (li ,(a "https://pboyd.io/posts/how-not-to-store-passwords/"
      "How NOT to Store Passwords")))
   (p "Despite what people tell you, I " (i "do") " think we should
use randomly-generated passwords. We aren't exactly known for coming
up with secure passwords ourselves. Go ahead and try '" (b "12345") "'
on any of my old accounts and you'll be logged in! I'd honestly like
to be able to come up with secure, memorable passwords every day, but
it's so much easier to let my computer do it for me.")
   (p "As always, drop me a comment by "
    ,(a "/index.html#contact"
     "contacting me directly")
    "!")))
