;; The Spook configuration for muto.ca
;; 
(template
 `((html
    (@ (lang "en")) ; HTML tag start, with lang attribute.
    (head ; Start head
     (meta (@ (name    "viewport") ; Better text size for mobile.
	      (content "width=device-width"))) 
     (meta (@ (charset "utf-8"))) ; Character set.
     (title ,title) ; Title of the page, defined in post files.
     (link (@ (type "text/css")
	      (rel  "stylesheet")
	      (href "/css/style.css")))
     ;;Inline stylesheets, if you have special pages.
     ,(if (null? style)
	  '()
	  `((style ,style))))
    (cc "If you're gonna look at the source code,
follow the hidden link in style.css")
    (body ;; MAIN CONTENT OF PAGE
     (div (@ (class "nav"))
	  (p
	   ,(a "/index.html"  "Muto's Shack") " - "
	   ,(a "/b"  "Weblog")  " - "
	   ,(a "/p"  "Projects") " - "
	   ,(a "https://functional.cafe/@MutoShack" "Microblog"))
	  (hr)
	  ;; If we didn't specify a #:date tag to our post,
	  ;; don't try to print the title/date.
	  ,(if (null? date)
	       '() ; Print nothing.
	       `((h1 ,title)  ; Display the title in a header.
		 (h2 ,date))) ; And the date below it.
	  ,body
	  (hr)
	  (div (@ (class "footer"))
	       (p "Made with "
		  ,(a "/p/spook.html"
		      "Spook!")      " | "
		  ,(a "https://gitlab.com/MutoShack/spook-site"
		      "Source code") " | "
		  ,(a "/index.html#contact"
		      "Hire me."))))))))

(blog ; My 'blog' index.
 :title "Muto's Blog"
 :msg   "Recent Posts"
 :path  "b/")

(blog ; My 'projects' index.
 :title "My Projects"
 :msg   "Projects"
 :path  "p/")

(directories
 :pages  "pages/"  ; Where my SXML files are.
 :output "site/"   ; Where my HTML files go.
 :assets "css/")   ; Assets like images and CSS.
