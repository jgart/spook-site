#!/usr/bin/env -S guile -L .
!#

;; Spook, the static website generator.
;; Source code.
(use-modules
 (simple)   ; For HTML generation.
 (ice-9 optargs) ; For :keys.
 (ice-9 ftw))    ; For file listings and such.

;; We want to be able to accept :keywords, which are cleaner than
;; #:keywords, so we set this variable to 'prefix
(read-set! keywords 'prefix)

;; We're going to create some 'dummy' vars here. They'll hold
;; important data in the future. Also they hold default values.
(define input-folder  "pages/")  ; Where our SXML documents will go.
(define output-folder "site/")   ; Where our stuff will end up
(define asset-folder  "assets/") ; Where our CSS / images are.

;; Function for defining directories.
;; (directories) is called by the user in spook.scm
;; if they want to change the defaults.
(define* (directories
	  #:key
	  (pages  "pages/")
	  (output "site/")
	  (assets "assets/"))
  (set! input-folder  pages)
  (set! output-folder output)
  (set! asset-folder  assets))

;; Some shortcuts for common SXML tags.
;; Because it's cleaner.
(define (a link txt)   ; Hyperlink shortcut.
  `(a (@ (href ,link)) ; ,(a "link" "text")
      ,txt))
(define* (img src #:optional alt)  ; Image shortcut.
  `(img (@ (src ,src)  ; ,(img "image" "Alt text")
	   (alt ,alt))))

(define-syntax put ; Simpler 'display'
  (syntax-rules ()
    ((put str ...)
     (display (string-append str ... "\n")))))

(define temp '())       ; The template SXML for new pages.
(define (template body) ; Function for creating a page.
  (set! temp body)
  body)

;; The 'ls' command. Not really POSIX, just a shortcut.
;; I list dirs a lot, is all.
(define-syntax ls
  (syntax-rules ()
    ((ls dir ...) ; strcat all strings together.
     (cddr (scandir (string-append dir ...))))))

;; Tests to see if a file is SXML or HTML
(define (is-html? file)
  (or
   (string-contains-ci file ".html")
   (string-contains-ci file ".htm")))
(define (is-sxml? file)
  (or
   (string-contains-ci file ".scm")
   (string-contains-ci file ".sxml")))

(define current-title '())  ; Title of a blog post.
(define current-date  '())  ; Date of a blog post.

;; Function for creating new pages.
;; (page #:title "Title" #:date "Jan 1" #:body `((p hello)))
(define* (page
	  #:key
	  (title "~Squirrel in a Top Hat~")
	  (date  '()) ; String of text, "Oct 15, 2015"
	  (style '()) ; Inline style, as a string.
	  (body  '(p "UwU What's This?")))
  (set! current-title title)
  (set! current-date  date)
  (include "spook.scm")
  temp)

;; Transform every SXML file in a folder.
;; All the SXML files will be converted to HTML.
;; All the resulting HTML files will be thrown into
;; the output folder, specified by the user, default to 'site/'
(define (generate-all-in-folder dir)
  "Transform .scm files in DIR to .html files in OUTPUT-FOLDER"
  (define count 0) ; So we can tell the user how many files we found.
  ;; (do "local variable" "what to do after every loop"
  ;;     "Series of procedures to loop over")
  (do ((listing (ls dir) (cdr listing)))
      ((null? listing) '())
    (let ((file (string-append dir (car listing))))
      (unless (file-is-directory? file)
	;; Instead of /home/me/spook/pages/dir1,
	;;            /home/me/spook/site/dir1
	;; Kind of a bigol' mess.
	(let ((out-folder
	       (string-append
		output-folder ;site/
		(string-drop
		 (string-append (canonicalize-path dir) "/")
		 (string-length
		  (canonicalize-path input-folder))))))
	  ;;	    (put file " is an SXML file. Reading...")
	  (set! count (1+ count))
	  (unless (file-exists? out-folder)
	    (mkdir out-folder)) ; Make sure the output folder exists.
	  (if (is-sxml? file)
	  (let ((port
		 (open-output-file ; The outputted HTML file
		  (string-append
		   out-folder ; site/ is the usual.
		   (basename file ".scm")
		   ".html")))) ; Add HTML, so test.scm -> test.html
	    (display "<!DOCTYPE html>" port) ;That kinda stinks.
	    (sxml->xml (load file) port) ; Main conversion.
	    (display "\n" port)))
	  (if (is-html? file)
	  (let ((port
		 (open-output-file ; The outputted HTML file
		  (string-append
		   out-folder ; site/ is the usual.
		   (basename file)))))
	    (display "<!DOCTYPE html>" port)
	    (sxml->xml
	     (page #:title "Spook"
		   #:body (xml->sxml (open-input-file file))) port)
	    (display "\n" port)))))))
	  
  (display "  \x1B[1;91mReading ")
  (display count)
  (display " files. \x1B[m")
  (newline))

;; We want to be able to gnerate ALL files, even in subdirectories.
;; With (recursive-generation) we enter any folder and run
;; (generate-all-in-folder) within it. We also check the folder
;; for any subfolders and it goes on until there are no more folders
;; to look in.
(define (recursive-generation in-dir)
  "Call (generate-all-in-folder) on every folder."
  (do ((listing (ls in-dir) (cdr listing)))
      ((null? listing) '())
    (let ((file (string-append in-dir (car listing))))
      (cond
       ((file-is-directory? file)
	(put "\x1B[93m" file " is a directory. Entering...")
	(set! file (string-append file "/")) ;Add / to dir
	(generate-all-in-folder file)
	(recursive-generation file)))))
  
  ;; Just this once, I'm calling an external program.
  ;; If you know of any ways to copy a directory in Guile
  ;; (without having to write a whole new procedure)
  ;; let me know: shack at muto dot ca
  (system (string-append "cp -r " asset-folder
			 " "      output-folder)))

(define (blog-style title msg ul port)
  (sxml->xml
   (page
    #:title title
    #:body
    `(,(if (null? msg)'()
	   `((h2 ,msg)))
      (ul ,ul))) port))

;; Blog template. Contains a <ul> of blog entries.
(define (generate-blog title msg path)
  "Create an INDEX.HTML file for our blog directory."
  (do ((listing (ls input-folder path) (cdr listing))
       (blog-port
	(open-output-file
	 (string-append output-folder path "index.html")))
       (my-ul '())) ; Where our <ul> <li> ... </li> </ul> of posts will go.
      ((null? listing) ; All done, print the full page as a site page.
       (blog-style title msg my-ul blog-port))
    (let ((file (string-append
		 input-folder
		 path (car listing))))
      (unless (file-is-directory? file)
	(load file)
	(set! my-ul
	  (cons
	   `(li ,(a (string-append
		     (basename file ".scm")
		     ".html")
		    current-title)
		,(if (null? current-date) '()
		     (string-append " - " current-date)))
	   my-ul))))))

;; The blog creation functionality.
;; We want to be able to specify new blog directories
;; whenever we want in spook.scm, like
;; (blog #:title "hi" #:msg "Stuff" #:path "random/")
(define blogs '())
(define* (blog #:key
	       (title "My Blog")
	       (msg   "Recent Posts")
	       (path  "blog/"))
  (set! blogs (cons (list title msg path)  blogs)))

(define (gen-blogs)
  "Loop through the list of blogs, generate all blogs."
  (do ((list-of-blogs blogs (cdr list-of-blogs)))
      ((null? list-of-blogs) '())
    (let ((title  (caar   list-of-blogs))
	  (msg    (cadar  list-of-blogs))
	  (path   (caddar list-of-blogs)))
      (generate-blog title msg path))))

(put "--------------------------------------------------")
(put "Spook, The Static Site Generator v5.2\n")
(put "\x1B[93mEntering " input-folder)
(generate-all-in-folder input-folder) ; Generate the toplevel folder.
(recursive-generation   input-folder) ; Generate all subfolders.
(gen-blogs)
(put "Website generated in: " output-folder)
